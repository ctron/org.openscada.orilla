/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.ui.chart.model.ChartModel;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composite Archive Quality Series</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openscada.ui.chart.model.ChartModel.CompositeArchiveQualitySeries#getThreshold <em>Threshold</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openscada.ui.chart.model.ChartModel.ChartPackage#getCompositeArchiveQualitySeries()
 * @model
 * @generated
 */
public interface CompositeArchiveQualitySeries extends DataSeries
{
    /**
     * Returns the value of the '<em><b>Threshold</b></em>' attribute.
     * The default value is <code>"0.8"</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Threshold</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Threshold</em>' attribute.
     * @see #setThreshold(double)
     * @see org.openscada.ui.chart.model.ChartModel.ChartPackage#getCompositeArchiveQualitySeries_Threshold()
     * @model default="0.8" required="true"
     * @generated
     */
    double getThreshold ();

    /**
     * Sets the value of the '{@link org.openscada.ui.chart.model.ChartModel.CompositeArchiveQualitySeries#getThreshold <em>Threshold</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Threshold</em>' attribute.
     * @see #getThreshold()
     * @generated
     */
    void setThreshold ( double value );

} // CompositeArchiveQualitySeries
