/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.ui.chart.model.ChartModel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XAxis Controller</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openscada.ui.chart.model.ChartModel.XAxisController#getAxis <em>Axis</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openscada.ui.chart.model.ChartModel.ChartPackage#getXAxisController()
 * @model abstract="true"
 * @generated
 */
public interface XAxisController extends Controller
{
    /**
     * Returns the value of the '<em><b>Axis</b></em>' reference list.
     * The list contents are of type {@link org.openscada.ui.chart.model.ChartModel.XAxis}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Axis</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Axis</em>' reference list.
     * @see org.openscada.ui.chart.model.ChartModel.ChartPackage#getXAxisController_Axis()
     * @model
     * @generated
     */
    EList<XAxis> getAxis ();

} // XAxisController
