/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.ui.chart.model.ChartModel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Controller</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.openscada.ui.chart.model.ChartModel.ChartPackage#getController()
 * @model abstract="true"
 * @generated
 */
public interface Controller extends EObject
{
} // Controller
