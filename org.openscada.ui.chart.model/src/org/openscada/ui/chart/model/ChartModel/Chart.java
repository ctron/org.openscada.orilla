/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.ui.chart.model.ChartModel;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.graphics.RGB;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Chart</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openscada.ui.chart.model.ChartModel.Chart#getTitle <em>Title</em>}</li>
 *   <li>{@link org.openscada.ui.chart.model.ChartModel.Chart#isShowCurrentTimeRuler <em>Show Current Time Ruler</em>}</li>
 *   <li>{@link org.openscada.ui.chart.model.ChartModel.Chart#getBackgroundColor <em>Background Color</em>}</li>
 *   <li>{@link org.openscada.ui.chart.model.ChartModel.Chart#getBottom <em>Bottom</em>}</li>
 *   <li>{@link org.openscada.ui.chart.model.ChartModel.Chart#getTop <em>Top</em>}</li>
 *   <li>{@link org.openscada.ui.chart.model.ChartModel.Chart#getLeft <em>Left</em>}</li>
 *   <li>{@link org.openscada.ui.chart.model.ChartModel.Chart#getRight <em>Right</em>}</li>
 *   <li>{@link org.openscada.ui.chart.model.ChartModel.Chart#getSelectedYAxis <em>Selected YAxis</em>}</li>
 *   <li>{@link org.openscada.ui.chart.model.ChartModel.Chart#getSelectedXAxis <em>Selected XAxis</em>}</li>
 *   <li>{@link org.openscada.ui.chart.model.ChartModel.Chart#getInputs <em>Inputs</em>}</li>
 *   <li>{@link org.openscada.ui.chart.model.ChartModel.Chart#isMutable <em>Mutable</em>}</li>
 *   <li>{@link org.openscada.ui.chart.model.ChartModel.Chart#getControllers <em>Controllers</em>}</li>
 *   <li>{@link org.openscada.ui.chart.model.ChartModel.Chart#isHoverable <em>Hoverable</em>}</li>
 *   <li>{@link org.openscada.ui.chart.model.ChartModel.Chart#getProfiles <em>Profiles</em>}</li>
 *   <li>{@link org.openscada.ui.chart.model.ChartModel.Chart#getActiveProfile <em>Active Profile</em>}</li>
 *   <li>{@link org.openscada.ui.chart.model.ChartModel.Chart#getProfileSwitcherType <em>Profile Switcher Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openscada.ui.chart.model.ChartModel.ChartPackage#getChart()
 * @model
 * @generated
 */
public interface Chart extends EObject
{
    /**
     * Returns the value of the '<em><b>Title</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Title</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Title</em>' attribute.
     * @see #setTitle(String)
     * @see org.openscada.ui.chart.model.ChartModel.ChartPackage#getChart_Title()
     * @model
     * @generated
     */
    String getTitle ();

    /**
     * Sets the value of the '{@link org.openscada.ui.chart.model.ChartModel.Chart#getTitle <em>Title</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Title</em>' attribute.
     * @see #getTitle()
     * @generated
     */
    void setTitle ( String value );

    /**
     * Returns the value of the '<em><b>Show Current Time Ruler</b></em>' attribute.
     * The default value is <code>"true"</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Show Current Time Ruler</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Show Current Time Ruler</em>' attribute.
     * @see #setShowCurrentTimeRuler(boolean)
     * @see org.openscada.ui.chart.model.ChartModel.ChartPackage#getChart_ShowCurrentTimeRuler()
     * @model default="true" required="true"
     * @generated
     */
    boolean isShowCurrentTimeRuler ();

    /**
     * Sets the value of the '{@link org.openscada.ui.chart.model.ChartModel.Chart#isShowCurrentTimeRuler <em>Show Current Time Ruler</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Show Current Time Ruler</em>' attribute.
     * @see #isShowCurrentTimeRuler()
     * @generated
     */
    void setShowCurrentTimeRuler ( boolean value );

    /**
     * Returns the value of the '<em><b>Background Color</b></em>' attribute.
     * The default value is <code>"#FFFFFF"</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Background Color</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Background Color</em>' attribute.
     * @see #setBackgroundColor(RGB)
     * @see org.openscada.ui.chart.model.ChartModel.ChartPackage#getChart_BackgroundColor()
     * @model default="#FFFFFF" dataType="org.openscada.ui.chart.model.ChartModel.RGB" required="true"
     *        extendedMetaData="name='backgroundColor'"
     * @generated
     */
    RGB getBackgroundColor ();

    /**
     * Sets the value of the '{@link org.openscada.ui.chart.model.ChartModel.Chart#getBackgroundColor <em>Background Color</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Background Color</em>' attribute.
     * @see #getBackgroundColor()
     * @generated
     */
    void setBackgroundColor ( RGB value );

    /**
     * Returns the value of the '<em><b>Bottom</b></em>' containment reference list.
     * The list contents are of type {@link org.openscada.ui.chart.model.ChartModel.XAxis}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Bottom</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Bottom</em>' containment reference list.
     * @see org.openscada.ui.chart.model.ChartModel.ChartPackage#getChart_Bottom()
     * @model containment="true" resolveProxies="true"
     * @generated
     */
    EList<XAxis> getBottom ();

    /**
     * Returns the value of the '<em><b>Top</b></em>' containment reference list.
     * The list contents are of type {@link org.openscada.ui.chart.model.ChartModel.XAxis}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Top</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Top</em>' containment reference list.
     * @see org.openscada.ui.chart.model.ChartModel.ChartPackage#getChart_Top()
     * @model containment="true" resolveProxies="true"
     * @generated
     */
    EList<XAxis> getTop ();

    /**
     * Returns the value of the '<em><b>Left</b></em>' containment reference list.
     * The list contents are of type {@link org.openscada.ui.chart.model.ChartModel.YAxis}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Left</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Left</em>' containment reference list.
     * @see org.openscada.ui.chart.model.ChartModel.ChartPackage#getChart_Left()
     * @model containment="true" resolveProxies="true"
     * @generated
     */
    EList<YAxis> getLeft ();

    /**
     * Returns the value of the '<em><b>Right</b></em>' containment reference list.
     * The list contents are of type {@link org.openscada.ui.chart.model.ChartModel.YAxis}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Right</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Right</em>' containment reference list.
     * @see org.openscada.ui.chart.model.ChartModel.ChartPackage#getChart_Right()
     * @model containment="true" resolveProxies="true"
     * @generated
     */
    EList<YAxis> getRight ();

    /**
     * Returns the value of the '<em><b>Selected YAxis</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Selected YAxis</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Selected YAxis</em>' reference.
     * @see #setSelectedYAxis(YAxis)
     * @see org.openscada.ui.chart.model.ChartModel.ChartPackage#getChart_SelectedYAxis()
     * @model
     * @generated
     */
    YAxis getSelectedYAxis ();

    /**
     * Sets the value of the '{@link org.openscada.ui.chart.model.ChartModel.Chart#getSelectedYAxis <em>Selected YAxis</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Selected YAxis</em>' reference.
     * @see #getSelectedYAxis()
     * @generated
     */
    void setSelectedYAxis ( YAxis value );

    /**
     * Returns the value of the '<em><b>Selected XAxis</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Selected XAxis</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Selected XAxis</em>' reference.
     * @see #setSelectedXAxis(XAxis)
     * @see org.openscada.ui.chart.model.ChartModel.ChartPackage#getChart_SelectedXAxis()
     * @model
     * @generated
     */
    XAxis getSelectedXAxis ();

    /**
     * Sets the value of the '{@link org.openscada.ui.chart.model.ChartModel.Chart#getSelectedXAxis <em>Selected XAxis</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Selected XAxis</em>' reference.
     * @see #getSelectedXAxis()
     * @generated
     */
    void setSelectedXAxis ( XAxis value );

    /**
     * Returns the value of the '<em><b>Inputs</b></em>' containment reference list.
     * The list contents are of type {@link org.openscada.ui.chart.model.ChartModel.DataSeries}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Inputs</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Inputs</em>' containment reference list.
     * @see org.openscada.ui.chart.model.ChartModel.ChartPackage#getChart_Inputs()
     * @model containment="true" resolveProxies="true"
     * @generated
     */
    EList<DataSeries> getInputs ();

    /**
     * Returns the value of the '<em><b>Mutable</b></em>' attribute.
     * The default value is <code>"true"</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Mutable</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Mutable</em>' attribute.
     * @see #setMutable(boolean)
     * @see org.openscada.ui.chart.model.ChartModel.ChartPackage#getChart_Mutable()
     * @model default="true" required="true"
     * @generated
     */
    boolean isMutable ();

    /**
     * Sets the value of the '{@link org.openscada.ui.chart.model.ChartModel.Chart#isMutable <em>Mutable</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Mutable</em>' attribute.
     * @see #isMutable()
     * @generated
     */
    void setMutable ( boolean value );

    /**
     * Returns the value of the '<em><b>Controllers</b></em>' containment reference list.
     * The list contents are of type {@link org.openscada.ui.chart.model.ChartModel.Controller}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Controllers</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Controllers</em>' containment reference list.
     * @see org.openscada.ui.chart.model.ChartModel.ChartPackage#getChart_Controllers()
     * @model containment="true" resolveProxies="true"
     * @generated
     */
    EList<Controller> getControllers ();

    /**
     * Returns the value of the '<em><b>Hoverable</b></em>' attribute.
     * The default value is <code>"true"</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Hoverable</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Hoverable</em>' attribute.
     * @see #setHoverable(boolean)
     * @see org.openscada.ui.chart.model.ChartModel.ChartPackage#getChart_Hoverable()
     * @model default="true" required="true"
     * @generated
     */
    boolean isHoverable ();

    /**
     * Sets the value of the '{@link org.openscada.ui.chart.model.ChartModel.Chart#isHoverable <em>Hoverable</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Hoverable</em>' attribute.
     * @see #isHoverable()
     * @generated
     */
    void setHoverable ( boolean value );

    /**
     * Returns the value of the '<em><b>Profiles</b></em>' containment reference list.
     * The list contents are of type {@link org.openscada.ui.chart.model.ChartModel.Profile}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Profiles</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Profiles</em>' containment reference list.
     * @see org.openscada.ui.chart.model.ChartModel.ChartPackage#getChart_Profiles()
     * @model containment="true" resolveProxies="true"
     * @generated
     */
    EList<Profile> getProfiles ();

    /**
     * Returns the value of the '<em><b>Active Profile</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Active Profile</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Active Profile</em>' reference.
     * @see #setActiveProfile(Profile)
     * @see org.openscada.ui.chart.model.ChartModel.ChartPackage#getChart_ActiveProfile()
     * @model
     * @generated
     */
    Profile getActiveProfile ();

    /**
     * Sets the value of the '{@link org.openscada.ui.chart.model.ChartModel.Chart#getActiveProfile <em>Active Profile</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Active Profile</em>' reference.
     * @see #getActiveProfile()
     * @generated
     */
    void setActiveProfile ( Profile value );

    /**
     * Returns the value of the '<em><b>Profile Switcher Type</b></em>' attribute.
     * The default value is <code>"BUTTON"</code>.
     * The literals are from the enumeration {@link org.openscada.ui.chart.model.ChartModel.ProfileSwitcherType}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Profile Switcher Type</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Profile Switcher Type</em>' attribute.
     * @see org.openscada.ui.chart.model.ChartModel.ProfileSwitcherType
     * @see #setProfileSwitcherType(ProfileSwitcherType)
     * @see org.openscada.ui.chart.model.ChartModel.ChartPackage#getChart_ProfileSwitcherType()
     * @model default="BUTTON" required="true"
     * @generated
     */
    ProfileSwitcherType getProfileSwitcherType ();

    /**
     * Sets the value of the '{@link org.openscada.ui.chart.model.ChartModel.Chart#getProfileSwitcherType <em>Profile Switcher Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Profile Switcher Type</em>' attribute.
     * @see org.openscada.ui.chart.model.ChartModel.ProfileSwitcherType
     * @see #getProfileSwitcherType()
     * @generated
     */
    void setProfileSwitcherType ( ProfileSwitcherType value );

} // Chart
