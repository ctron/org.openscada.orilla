/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.ui.chart.model.ChartModel;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Time Now Action</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.openscada.ui.chart.model.ChartModel.ChartPackage#getTimeNowAction()
 * @model
 * @generated
 */
public interface TimeNowAction extends XAxisController
{
} // TimeNowAction
