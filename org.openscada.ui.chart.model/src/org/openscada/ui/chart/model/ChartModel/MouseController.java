/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.ui.chart.model.ChartModel;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mouse Controller</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.openscada.ui.chart.model.ChartModel.ChartPackage#getMouseController()
 * @model
 * @generated
 */
public interface MouseController extends Controller
{
} // MouseController
