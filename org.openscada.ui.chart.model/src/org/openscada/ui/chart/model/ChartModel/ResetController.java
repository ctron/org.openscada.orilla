/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.ui.chart.model.ChartModel;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reset Controller</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.openscada.ui.chart.model.ChartModel.ChartPackage#getResetController()
 * @model
 * @generated
 */
public interface ResetController extends Controller
{
} // ResetController
