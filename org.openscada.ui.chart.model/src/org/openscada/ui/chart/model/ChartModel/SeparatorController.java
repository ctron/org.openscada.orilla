/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.ui.chart.model.ChartModel;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Separator Controller</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.openscada.ui.chart.model.ChartModel.ChartPackage#getSeparatorController()
 * @model
 * @generated
 */
public interface SeparatorController extends Controller
{
} // SeparatorController
