/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.sec.ui.providers.pkcs12;

import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.list.WritableList;
import org.eclipse.ui.statushandlers.StatusManager;
import org.openscada.sec.ui.providers.KeyProvider;
import org.openscada.sec.ui.providers.KeyProviderFactory;
import org.openscada.ui.utils.status.StatusHelper;

public class FactoryImpl implements KeyProviderFactory
{

    private final Set<String> files = new HashSet<String> ();

    private Realm realm;

    private WritableList list;

    private final Preferences prefs;

    public FactoryImpl ()
    {
        this.prefs = Preferences.userNodeForPackage ( FactoryImpl.class ).node ( "files" );

        try
        {
            for ( final String child : this.prefs.childrenNames () )
            {
                final Preferences childNode = this.prefs.node ( child );
                final String fileName = childNode.get ( "file", null );
                if ( fileName != null )
                {
                    this.files.add ( fileName );
                }
            }
        }
        catch ( final BackingStoreException e )
        {
            StatusManager.getManager ().handle ( StatusHelper.convertStatus ( Activator.PLUGIN_ID, e ) );
        }
    }

    public void addFile ( final String fileName ) throws Exception
    {
        final File file = new File ( fileName ).getAbsoluteFile ();

        final Preferences newChild = this.prefs.node ( UUID.randomUUID ().toString () );
        newChild.put ( "file", file.toString () );
        newChild.flush ();

        this.files.add ( fileName );
        loadFile ( fileName );
    }

    public void remove ( final KeyProvider keyProvider ) throws Exception
    {
        if ( ! ( keyProvider instanceof PKCS12KeyProvider ) )
        {
            return;
        }

        if ( !this.list.remove ( keyProvider ) )
        {
            return;
        }

        final File file = ( (PKCS12KeyProvider)keyProvider ).getFile ();

        keyProvider.dispose ();

        removeFile ( file.getAbsoluteFile () );
    }

    private void removeFile ( final File file ) throws Exception
    {
        for ( final String childName : this.prefs.childrenNames () )
        {
            final Preferences child = this.prefs.node ( childName );
            final String fileName = new File ( child.get ( "file", null ) ).getAbsolutePath ();
            if ( file.getAbsolutePath ().equals ( fileName ) )
            {
                child.removeNode ();
            }
        }
        this.prefs.flush ();
    }

    @Override
    public void init ( final Realm realm )
    {
        this.realm = realm;
        this.list = new WritableList ();

        for ( final String fileName : this.files )
        {
            loadFile ( fileName );
        }
    }

    private void loadFile ( final String fileName )
    {
        try
        {
            this.list.add ( new PKCS12KeyProvider ( this.realm, new File ( fileName ) ) );
        }
        catch ( final Exception e )
        {
            StatusManager.getManager ().handle ( StatusHelper.convertStatus ( Activator.PLUGIN_ID, e ) );
        }
    }

    @Override
    public void dispose ()
    {
        for ( final Object o : this.list )
        {
            ( (PKCS12KeyProvider)o ).dispose ();
        }
        this.list.dispose ();
    }

    @Override
    public String toString ()
    {
        return "PKCS12";
    }

    @Override
    public IObservableList getKeyProviders ()
    {
        return this.list;
    }

}
