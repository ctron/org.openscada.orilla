/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.sec.ui.providers.pkcs12;

import java.io.File;
import java.security.KeyStore;

import org.eclipse.core.databinding.observable.Realm;
import org.openscada.sec.callback.CallbackHandler;
import org.openscada.sec.ui.providers.AbstractKeyStoreKeyProvider;

public class PKCS12KeyProvider extends AbstractKeyStoreKeyProvider
{

    private final File file;

    public PKCS12KeyProvider ( final Realm realm, final File file ) throws Exception
    {
        super ( realm );
        this.file = file;

        setKeyStore ( KeyStore.getInstance ( "PKCS12" ) );

        /*
         * we cannot try an initial load without password since java succeeds and gives
         * us the locked key, but refuses to give us the certificate then.
        */
    }

    @Override
    public String toString ()
    {
        return String.format ( "%s", this.file );
    }

    @Override
    public void unlock ( final CallbackHandler callbackHandler ) throws Exception
    {
        load ( this.file, callbackHandler );
    }

    public File getFile ()
    {
        return this.file;
    }
}
