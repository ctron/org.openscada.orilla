/*
 * This file is part of the OpenSCADA project
 * 
 * Copyright (C) 2006-2010 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * OpenSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.ae.ui.views;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS
{
    private static final ResourceBundle RESOURCE_BUNDLE;

    private static final String BUNDLE_NAME = "org.openscada.ae.ui.views.messages"; //$NON-NLS-1$

    // public static String Acknowledge;

    public static String ID;

    public static String AckTimestamp;

    public static String AckUser;

    public static String State;

    public static String Timestamp;

    public static String Value;

    public static String Item;

    public static String ItemDescription;

    public static String Message;

    public static String MonitorsViewTable_LastFailValue;

    public static String MonitorsViewTable_StatusTimestamp;

    public static String MonitorsViewTable_Severity;

    public static String from;

    public static String to;

    public static String search_for_events;

    public static String search_for_events_description;

    // public static String QBE;

    // public static String free_form_query;

    public static String filter_must_not_be_null;

    public static String custom_field;

    public static String add_assertion;

    public static String not;

    public static String argument;

    public static String remove;

    public static String a_condition;

    public static String add_or_condition;

    public static String clear;

    public static String sourceTimestamp;

    public static String entryTimestamp;

    public static String message;

    public static String monitorType;

    public static String eventType;

    public static String item;

    public static String value;

    public static String priority;

    public static String source;

    public static String actorType;

    public static String actorName;

    public static String component;

    public static String system;

    public static String hive;

    public static String location;

    public static String messageSource;

    public static String comment;

    static
    {
        // initialize resource bundle
        NLS.initializeMessages ( BUNDLE_NAME, Messages.class );
        RESOURCE_BUNDLE = ResourceBundle.getBundle ( BUNDLE_NAME );
    }

    private Messages ()
    {
    }

    public static String getString ( final String key )
    {
        try
        {
            return RESOURCE_BUNDLE.getString ( key );
        }
        catch ( final MissingResourceException e )
        {
            return key;
        }
    }
}
