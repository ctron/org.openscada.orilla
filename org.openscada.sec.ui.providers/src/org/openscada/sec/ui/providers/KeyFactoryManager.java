/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.sec.ui.providers;

import java.util.Collection;

import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.list.WritableList;

public class KeyFactoryManager
{
    private final WritableList list;

    public KeyFactoryManager ( final Realm realm )
    {
        this.list = new WritableList ( realm );

        final Collection<KeyProviderFactory> factories = Helper.createFactories ();
        for ( final KeyProviderFactory factory : factories )
        {
            factory.init ( realm );
        }

        this.list.addAll ( factories );
    }

    public WritableList getList ()
    {
        return this.list;
    }

    public void dispose ()
    {
        for ( final Object o : this.list )
        {
            ( (KeyProviderFactory)o ).dispose ();
        }
        this.list.dispose ();
    }
}
