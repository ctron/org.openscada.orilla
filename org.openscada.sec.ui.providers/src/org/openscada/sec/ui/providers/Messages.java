/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.sec.ui.providers;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS
{
    private static final String BUNDLE_NAME = "org.openscada.sec.ui.providers.messages"; //$NON-NLS-1$

    public static String KeySelectorDialog_ButtonUnlock_Text;

    public static String KeySelectorDialog_Callback_Description;

    public static String KeySelectorDialog_Callback_Title;

    public static String KeySelectorDialog_Error_Text;

    public static String KeySelectorDialog_Error_Title;

    public static String KeySelectorDialog_Remember_Text;

    public static String KeySelectorDialog_RememberLabel_Text_Forever;

    public static String KeySelectorDialog_RememberLabel_Text_Minute;

    public static String KeySelectorDialog_RememberLabel_Text_Minutes;
    static
    {
        // initialize resource bundle
        NLS.initializeMessages ( BUNDLE_NAME, Messages.class );
    }

    private Messages ()
    {
    }
}
