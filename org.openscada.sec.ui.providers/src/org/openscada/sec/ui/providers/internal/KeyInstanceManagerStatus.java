/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.sec.ui.providers.internal;

import java.util.Date;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.menus.WorkbenchWindowControlContribution;
import org.openscada.sec.ui.providers.KeyInformation;
import org.openscada.sec.ui.providers.KeyInstanceManager;
import org.openscada.sec.ui.providers.KeyInstanceManager.StatusListener;

public class KeyInstanceManagerStatus extends WorkbenchWindowControlContribution implements StatusListener
{

    private Display display;

    private Label label;

    private KeyInformation key;

    private Date validUntil;

    private final Runnable updateRunner = new Runnable () {
        @Override
        public void run ()
        {
            updateTimeout ();
        }
    };

    public KeyInstanceManagerStatus ()
    {
    }

    public KeyInstanceManagerStatus ( final String id )
    {
        super ( id );
    }

    @Override
    protected Control createControl ( final Composite parent )
    {
        this.display = parent.getDisplay ();
        KeyInstanceManager.getInstance ( parent.getDisplay () ).addStatusListener ( this );

        final Composite wrapper = new Composite ( parent, SWT.NONE );
        wrapper.setLayout ( new GridLayout ( 1, false ) );

        this.label = new Label ( wrapper, SWT.NONE );

        final GridData gd = new GridData ( SWT.FILL, SWT.FILL, false, true );
        gd.widthHint = 150;

        this.label.setLayoutData ( gd );

        this.display.timerExec ( 500, this.updateRunner );

        updateLabel ();

        return wrapper;
    }

    protected void updateTimeout ()
    {
        if ( this.label.isDisposed () )
        {
            return;
        }

        this.display.timerExec ( 500, this.updateRunner );
        updateLabel ();
    }

    @Override
    public void defaultKeyChanged ( final KeyInformation key, final Date validUntil )
    {
        this.key = key;
        this.validUntil = validUntil;
        updateLabel ();
    }

    private void updateLabel ()
    {
        final StringBuilder sb = new StringBuilder ( "" );

        if ( this.key != null )
        {
            if ( this.validUntil != null )
            {
                final long seconds = ( this.validUntil.getTime () - System.currentTimeMillis () ) / 1000;
                if ( seconds > 0 )
                {
                    sb.append ( String.format ( "%d:%02d ", seconds / 60, seconds % 60 ) );
                }
            }
            sb.append ( "" + this.key );
        }

        this.label.setToolTipText ( sb.toString () );
        this.label.setText ( sb.toString () );
    }

    @Override
    public void dispose ()
    {
        KeyInstanceManager.getInstance ( this.display ).removeStatusListener ( this );
        super.dispose ();
    }

}
