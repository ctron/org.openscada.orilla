/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.sec.ui.providers.internal;

import org.eclipse.core.databinding.observable.IObservable;
import org.eclipse.core.databinding.observable.Observables;
import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.masterdetail.IObservableFactory;
import org.openscada.sec.ui.providers.KeyProvider;
import org.openscada.sec.ui.providers.KeyProviderFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FactoryImpl implements IObservableFactory
{

    private final static Logger logger = LoggerFactory.getLogger ( FactoryImpl.class );

    @Override
    public IObservable createObservable ( final Object target )
    {
        logger.debug ( "createObservable - {}", target );

        if ( target instanceof IObservableList )
        {
            return Observables.proxyObservableList ( (IObservableList)target );
        }
        else if ( target instanceof KeyProviderFactory )
        {
            return Observables.proxyObservableList ( ( (KeyProviderFactory)target ).getKeyProviders () );
        }
        else if ( target instanceof KeyProvider )
        {
            return Observables.proxyObservableList ( ( (KeyProvider)target ).getKeys () );
        }
        return null;
    }
}
