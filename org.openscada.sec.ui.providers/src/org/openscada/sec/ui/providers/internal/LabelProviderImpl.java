/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.sec.ui.providers.internal;

import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.observable.set.IObservableSet;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ResourceManager;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.graphics.Image;
import org.openscada.sec.ui.providers.KeyProvider;
import org.openscada.sec.ui.providers.KeyProviderFactory;
import org.openscada.sec.ui.providers.Locked;
import org.openscada.ui.databinding.ListeningStyledCellLabelProvider;

public final class LabelProviderImpl extends ListeningStyledCellLabelProvider
{
    private final ResourceManager manager;

    private final Image locked;

    public LabelProviderImpl ( final ResourceManager manager, final IObservableSet itemsThatNeedLabels )
    {
        super ( itemsThatNeedLabels );
        this.manager = manager;
        this.locked = this.manager.createImageWithDefault ( ImageDescriptor.createFromFile ( LabelProviderImpl.class, "icons/blocked.gif" ) );
     }

    @Override
    public void update ( final ViewerCell cell )
    {
        final Object ele = cell.getElement ();
        if ( ele instanceof KeyProviderFactory )
        {
            cell.setText ( ele.toString () );
        }
        else if ( ele instanceof KeyProvider )
        {
            final KeyProvider keyProvider = (KeyProvider)ele;
            cell.setText ( keyProvider.toString () );
            cell.setImage ( keyProvider.isLocked () ? this.locked : null );
        }
        else if ( ele instanceof org.openscada.sec.ui.providers.Key )
        {
            final org.openscada.sec.ui.providers.Key key = (org.openscada.sec.ui.providers.Key)ele;
            cell.setText ( key.toString () );
            cell.setImage ( key.isLocked () ? this.locked : null );
        }
    }

    @Override
    protected void addListenerTo ( final Object next )
    {
        if ( next instanceof KeyProvider )
        {
            listenTo ( next, BeanProperties.value ( KeyProvider.class, Locked.PROP_LOCKED ) );
        }
        else if ( next instanceof org.openscada.sec.ui.providers.Key )
        {
            listenTo ( next, BeanProperties.value ( org.openscada.sec.ui.providers.Key.class, Locked.PROP_LOCKED ) );
        }
    }

    @Override
    protected void removeListenerFrom ( final Object element )
    {
    }

}