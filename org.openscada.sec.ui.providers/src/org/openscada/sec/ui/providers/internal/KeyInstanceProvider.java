/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.sec.ui.providers.internal;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.AbstractSourceProvider;
import org.eclipse.ui.ISources;
import org.eclipse.ui.services.IServiceLocator;
import org.openscada.sec.ui.providers.KeyInformation;
import org.openscada.sec.ui.providers.KeyInstanceManager;
import org.openscada.sec.ui.providers.KeyInstanceManager.StatusListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KeyInstanceProvider extends AbstractSourceProvider implements StatusListener
{

    private final static Logger logger = LoggerFactory.getLogger ( KeyInstanceProvider.class );

    private static final String VAR_DEFAULT_KEY = "org.openscada.sec.ui.providers.KeyInstance.defaultKey";

    private static final String VAR_IS_DEFAULT_KEY = "org.openscada.sec.ui.providers.KeyInstance.isDefaultKey";

    private KeyInstanceManager mgr;

    public KeyInstanceProvider ()
    {
    }

    @Override
    public void initialize ( final IServiceLocator locator )
    {
        super.initialize ( locator );
        this.mgr = KeyInstanceManager.getInstance ( Display.getCurrent () );
        this.mgr.addStatusListener ( this );
    }

    @Override
    public void dispose ()
    {
        this.mgr.removeStatusListener ( this );
    }

    @SuppressWarnings ( { "rawtypes", "unchecked" } )
    @Override
    public Map getCurrentState ()
    {
        final Map result = new HashMap ();

        result.put ( VAR_DEFAULT_KEY, this.mgr.getDefaultKey () );
        result.put ( VAR_IS_DEFAULT_KEY, this.mgr.getDefaultKey () != null );

        return result;
    }

    @Override
    public String[] getProvidedSourceNames ()
    {
        return new String[] { VAR_DEFAULT_KEY };
    }

    @Override
    public void defaultKeyChanged ( final KeyInformation key, final Date validUntil )
    {
        logger.debug ( "Default key changed: {} -> {}", key, validUntil );

        fireSourceChanged ( ISources.WORKBENCH, getCurrentState () );
    }
}
