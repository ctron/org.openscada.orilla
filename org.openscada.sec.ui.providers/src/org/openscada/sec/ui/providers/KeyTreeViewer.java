/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.sec.ui.providers;

import org.eclipse.jface.databinding.viewers.ObservableListTreeContentProvider;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.LocalResourceManager;
import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.openscada.sec.ui.providers.internal.FactoryImpl;
import org.openscada.sec.ui.providers.internal.LabelProviderImpl;

public class KeyTreeViewer
{

    private final TreeViewer viewer;

    private final ObservableListTreeContentProvider contentProvider;

    private LabelProviderImpl labelProvider;

    private final LocalResourceManager resourceManager;

    public KeyTreeViewer ( final Composite parent )
    {
        this.resourceManager = new LocalResourceManager ( JFaceResources.getResources ( parent.getDisplay () ), parent );

        this.viewer = new TreeViewer ( parent );
        this.viewer.getControl ().setLayoutData ( new GridData ( SWT.FILL, SWT.FILL, true, true ) );

        this.viewer.setAutoExpandLevel ( AbstractTreeViewer.ALL_LEVELS );

        this.contentProvider = new ObservableListTreeContentProvider ( new FactoryImpl (), null );
        this.viewer.setContentProvider ( this.contentProvider );
        this.viewer.setLabelProvider ( this.labelProvider = new LabelProviderImpl ( this.resourceManager, this.contentProvider.getRealizedElements () ) );
    }

    public void setInput ( final KeyFactoryManager manager )
    {
        this.viewer.setInput ( manager.getList () );
    }

    public void setInput ( final KeyProviderFactory factory )
    {
        this.viewer.setInput ( factory );
    }

    public void dispose ()
    {
        this.labelProvider.dispose ();
        this.contentProvider.dispose ();
        this.viewer.getControl ().dispose ();
    }

    public Control getControl ()
    {
        return this.viewer.getControl ();
    }

    public TreeViewer getTreeViewer ()
    {
        return this.viewer;
    }

}
