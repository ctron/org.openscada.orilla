/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.sec.ui.providers;

import java.util.Collection;
import java.util.LinkedList;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.ui.statushandlers.StatusManager;
import org.openscada.sec.ui.providers.internal.Activator;

public class Helper
{
    private static final String ELE_FACTORY = "factory";

    private static final String EXTP_KEY_PROVIDER_FACTORY = "org.openscada.sec.ui.providers.key";

    private static final String ATTR_CLASS = "class";

    public static KeyProviderFactory findFactory ( final String id )
    {
        for ( final IConfigurationElement ele : Platform.getExtensionRegistry ().getConfigurationElementsFor ( EXTP_KEY_PROVIDER_FACTORY ) )
        {
            if ( !ELE_FACTORY.equals ( ele.getName () ) )
            {
                continue;
            }

            try
            {
                return (KeyProviderFactory)ele.createExecutableExtension ( ATTR_CLASS );
            }
            catch ( final CoreException e )
            {
                StatusManager.getManager ().handle ( e, Activator.PLUGIN_ID );
            }
        }

        return null;
    }

    public static Collection<KeyProviderFactory> createFactories ()
    {
        final Collection<KeyProviderFactory> result = new LinkedList<KeyProviderFactory> ();

        for ( final IConfigurationElement ele : Platform.getExtensionRegistry ().getConfigurationElementsFor ( EXTP_KEY_PROVIDER_FACTORY ) )
        {
            if ( !ELE_FACTORY.equals ( ele.getName () ) )
            {
                continue;
            }

            try
            {
                result.add ( (KeyProviderFactory)ele.createExecutableExtension ( ATTR_CLASS ) );
            }
            catch ( final CoreException e )
            {
                StatusManager.getManager ().handle ( e, Activator.PLUGIN_ID );
            }
        }

        return result;
    }
}
