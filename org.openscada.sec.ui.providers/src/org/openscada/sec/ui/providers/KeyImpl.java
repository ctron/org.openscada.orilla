/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.sec.ui.providers;

import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.concurrent.Future;

import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.ui.PlatformUI;
import org.openscada.sec.callback.Callback;
import org.openscada.sec.callback.CallbackHandler;
import org.openscada.sec.callback.Callbacks;
import org.openscada.sec.callback.PasswordCallback;
import org.openscada.sec.ui.providers.internal.Activator;
import org.openscada.ui.utils.status.StatusHelper;
import org.openscada.utils.beans.AbstractPropertyChange;
import org.openscada.utils.concurrent.FutureListener;
import org.openscada.utils.concurrent.NotifyFuture;

public class KeyImpl extends AbstractPropertyChange implements Key
{

    private final String alias;

    private final KeyStore keyStore;

    private java.security.Key key;

    private Certificate certificate;

    private boolean locked = true;

    public KeyImpl ( final String alias, final KeyStore keyStore, final String initialPassword )
    {
        this.alias = alias;
        this.keyStore = keyStore;

        try
        {
            performUnlock ( initialPassword );
        }
        catch ( final Exception e )
        {
            // silently ignore .. need manual operation
        }
    }

    @Override
    public boolean isLocked ()
    {
        return this.locked;
    }

    @Override
    public void dispose ()
    {
        this.key = null;
        this.certificate = null;
    }

    @Override
    public void unlock ( final CallbackHandler callbackHandler )
    {
        final NotifyFuture<Callback[]> future = Callbacks.callback ( callbackHandler, new PasswordCallback ( "Key password", 0 ) );
        future.addListener ( new FutureListener<Callback[]> () {

            @Override
            public void complete ( final Future<Callback[]> future )
            {
                try
                {
                    performUnlock ( ( (PasswordCallback)future.get ()[0] ).getPlainPassword () );
                }
                catch ( final Exception e )
                {
                    ErrorDialog.openError ( PlatformUI.getWorkbench ().getActiveWorkbenchWindow ().getShell (), "Error", "Failed to unlock", StatusHelper.convertStatus ( Activator.PLUGIN_ID, e ) );
                }
            }
        } );
    }

    protected void performUnlock ( final String password ) throws Exception
    {
        if ( !this.locked )
        {
            return;
        }

        this.key = this.keyStore.getKey ( this.alias, password != null ? password.toCharArray () : null );
        this.certificate = this.keyStore.getCertificate ( this.alias );
        setLocked ( false );
    }

    private void setLocked ( final boolean locked )
    {
        firePropertyChange ( PROP_LOCKED, this.locked, this.locked = locked );
    }

    @Override
    public String toString ()
    {
        if ( this.key != null && this.certificate instanceof X509Certificate )
        {
            return String.format ( "%s - %s", this.alias, ( (X509Certificate)this.certificate ).getSubjectX500Principal () );
        }
        else if ( this.key != null )
        {
            return String.format ( "%s - %s - %s", this.alias, this.key.getAlgorithm (), this.key.getFormat () );
        }
        else
        {
            return String.format ( "%s", this.alias );
        }
    }

    @Override
    public boolean isPrivate ()
    {
        return this.key != null;
    }

    @Override
    public java.security.Key getKey ()
    {
        return this.key;
    }

    @Override
    public Certificate getCertificate ()
    {
        return this.certificate;
    }

    @Override
    public String getName ()
    {
        return this.alias;
    }

}
