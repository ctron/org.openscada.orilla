/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.sec.ui.providers;

import java.security.Key;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;

import javax.security.auth.x500.X500Principal;

public class KeyInformation
{
    private final Key key;

    private final Certificate certificate;

    private final String alias;

    public KeyInformation ( final String alias, final Key key, final Certificate certificate )
    {
        this.alias = alias;
        this.key = key;
        this.certificate = certificate;
    }

    public String getAlias ()
    {
        return this.alias;
    }

    public Certificate getCertificate ()
    {
        return this.certificate;
    }

    public Key getKey ()
    {
        return this.key;
    }

    @Override
    public String toString ()
    {
        if ( this.certificate instanceof X509Certificate )
        {
            final X500Principal subject = ( (X509Certificate)this.certificate ).getSubjectX500Principal ();
            if ( subject != null )
            {
                return subject.toString ();
            }
        }
        if ( this.key != null )
        {
            return this.key.toString ();
        }
        else
        {
            return null;
        }
    }
}
