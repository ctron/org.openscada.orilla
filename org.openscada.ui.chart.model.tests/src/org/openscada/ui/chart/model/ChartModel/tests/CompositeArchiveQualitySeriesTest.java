/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.ui.chart.model.ChartModel.tests;

import junit.textui.TestRunner;

import org.openscada.ui.chart.model.ChartModel.ChartFactory;
import org.openscada.ui.chart.model.ChartModel.CompositeArchiveQualitySeries;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Composite Archive Quality Series</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class CompositeArchiveQualitySeriesTest extends DataSeriesTest
{

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static void main ( String[] args )
    {
        TestRunner.run ( CompositeArchiveQualitySeriesTest.class );
    }

    /**
     * Constructs a new Composite Archive Quality Series test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public CompositeArchiveQualitySeriesTest ( String name )
    {
        super ( name );
    }

    /**
     * Returns the fixture for this Composite Archive Quality Series test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected CompositeArchiveQualitySeries getFixture ()
    {
        return (CompositeArchiveQualitySeries)fixture;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp () throws Exception
    {
        setFixture ( ChartFactory.eINSTANCE.createCompositeArchiveQualitySeries () );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown () throws Exception
    {
        setFixture ( null );
    }

} //CompositeArchiveQualitySeriesTest
