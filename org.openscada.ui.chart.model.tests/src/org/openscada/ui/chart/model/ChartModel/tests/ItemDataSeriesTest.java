/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.ui.chart.model.ChartModel.tests;

import org.openscada.ui.chart.model.ChartModel.ItemDataSeries;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Item Data Series</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ItemDataSeriesTest extends DataSeriesTest
{

    /**
     * Constructs a new Item Data Series test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public ItemDataSeriesTest ( String name )
    {
        super ( name );
    }

    /**
     * Returns the fixture for this Item Data Series test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected ItemDataSeries getFixture ()
    {
        return (ItemDataSeries)fixture;
    }

} //ItemDataSeriesTest
