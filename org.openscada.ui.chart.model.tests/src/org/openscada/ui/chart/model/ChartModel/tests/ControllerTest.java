/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.ui.chart.model.ChartModel.tests;

import junit.framework.TestCase;

import org.openscada.ui.chart.model.ChartModel.Controller;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Controller</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ControllerTest extends TestCase
{

    /**
     * The fixture for this Controller test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected Controller fixture = null;

    /**
     * Constructs a new Controller test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public ControllerTest ( String name )
    {
        super ( name );
    }

    /**
     * Sets the fixture for this Controller test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void setFixture ( Controller fixture )
    {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Controller test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected Controller getFixture ()
    {
        return fixture;
    }

} //ControllerTest
