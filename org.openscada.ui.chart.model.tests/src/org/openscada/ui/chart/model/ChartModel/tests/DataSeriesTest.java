/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.ui.chart.model.ChartModel.tests;

import junit.framework.TestCase;

import org.openscada.ui.chart.model.ChartModel.DataSeries;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Data Series</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class DataSeriesTest extends TestCase
{

    /**
     * The fixture for this Data Series test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected DataSeries fixture = null;

    /**
     * Constructs a new Data Series test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public DataSeriesTest ( String name )
    {
        super ( name );
    }

    /**
     * Sets the fixture for this Data Series test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void setFixture ( DataSeries fixture )
    {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Data Series test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected DataSeries getFixture ()
    {
        return fixture;
    }

} //DataSeriesTest
