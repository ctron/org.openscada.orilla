/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.ui.chart.model.ChartModel.tests;

import junit.framework.TestCase;

import org.openscada.ui.chart.model.ChartModel.Axis;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Axis</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class AxisTest extends TestCase
{

    /**
     * The fixture for this Axis test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected Axis fixture = null;

    /**
     * Constructs a new Axis test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public AxisTest ( String name )
    {
        super ( name );
    }

    /**
     * Sets the fixture for this Axis test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void setFixture ( Axis fixture )
    {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Axis test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected Axis getFixture ()
    {
        return fixture;
    }

} //AxisTest
