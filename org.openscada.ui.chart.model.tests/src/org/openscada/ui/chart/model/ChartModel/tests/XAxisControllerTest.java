/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.ui.chart.model.ChartModel.tests;

import org.openscada.ui.chart.model.ChartModel.XAxisController;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>XAxis Controller</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class XAxisControllerTest extends ControllerTest
{

    /**
     * Constructs a new XAxis Controller test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public XAxisControllerTest ( String name )
    {
        super ( name );
    }

    /**
     * Returns the fixture for this XAxis Controller test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected XAxisController getFixture ()
    {
        return (XAxisController)fixture;
    }

} //XAxisControllerTest
