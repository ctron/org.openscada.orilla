/*
 * This file is part of the openSCADA project
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.ui.chart.viewer;

import org.openscada.chart.swt.ChartRenderer;
import org.openscada.ui.chart.model.ChartModel.Chart;
import org.openscada.ui.chart.model.ChartModel.XAxis;
import org.openscada.ui.chart.model.ChartModel.YAxis;

public class ChartContextImpl implements ChartContext
{
    private final AxisLocator<XAxis, XAxisViewer> xAxisLocator;

    private final AxisLocator<YAxis, YAxisViewer> yAxisLocator;

    private final ExtensionSpaceProvider extensionSpaceProvider;

    private final ChartRenderer chartRenderer;

    private final Chart chart;

    private final ResetHandler resetHandler;

    public ChartContextImpl ( final AxisLocator<XAxis, XAxisViewer> xAxisLocator, final AxisLocator<YAxis, YAxisViewer> yAxisLocator, final ExtensionSpaceProvider extensionSpaceProvider, final ChartRenderer chartRenderer, final Chart chart, final ResetHandler resetHandler )
    {
        this.resetHandler = resetHandler;
        this.xAxisLocator = xAxisLocator;
        this.yAxisLocator = yAxisLocator;
        this.extensionSpaceProvider = extensionSpaceProvider;
        this.chartRenderer = chartRenderer;
        this.chart = chart;
    }

    /* (non-Javadoc)
     * @see org.openscada.ui.chart.viewer.ChartContext#getxAxisLocator()
     */
    @Override
    public AxisLocator<XAxis, XAxisViewer> getxAxisLocator ()
    {
        return this.xAxisLocator;
    }

    /* (non-Javadoc)
     * @see org.openscada.ui.chart.viewer.ChartContext#getyAxisLocator()
     */
    @Override
    public AxisLocator<YAxis, YAxisViewer> getyAxisLocator ()
    {
        return this.yAxisLocator;
    }

    /* (non-Javadoc)
     * @see org.openscada.ui.chart.viewer.ChartContext#getExtensionSpaceProvider()
     */
    @Override
    public ExtensionSpaceProvider getExtensionSpaceProvider ()
    {
        return this.extensionSpaceProvider;
    }

    @Override
    public ChartRenderer getChartRenderer ()
    {
        return this.chartRenderer;
    }

    @Override
    public ResetHandler getResetHandler ()
    {
        return this.resetHandler;
    }

    @Override
    public Chart getChart ()
    {
        return this.chart;
    }
}
