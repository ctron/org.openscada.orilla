/*
 * This file is part of the openSCADA project
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.ui.chart.viewer.input.composite;

import org.openscada.chart.SeriesDataListener;
import org.openscada.chart.SeriesViewData;
import org.openscada.ui.chart.viewer.input.ArchiveInput;
import org.openscada.ui.chart.viewer.input.QuerySeriesData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CompositeSource
{

    private final static Logger logger = LoggerFactory.getLogger ( CompositeSource.class );

    private final SeriesDataListener listener = new SeriesDataListener () {

        @Override
        public void dataUpdate ( final long startTimestamp, final long endTimestamp )
        {
            handleDataUpdate ();
        }
    };

    private final QuerySeriesData data;

    private SeriesViewData viewData;

    private final CompositeQualityInput compositeQualityInput;

    public CompositeSource ( final CompositeQualityInput compositeQualityInput, final ArchiveInput archiveInput )
    {
        this.compositeQualityInput = compositeQualityInput;
        this.data = archiveInput.getData ();
        this.data.addListener ( this.listener );
    }

    public void dispose ()
    {
        this.data.removeListener ( this.listener );
    }

    protected void handleDataUpdate ()
    {
        logger.debug ( "Data update from: {}", this.data );

        this.viewData = this.data.getQualityData ();
        this.compositeQualityInput.performUpdate ();
    }

    public SeriesViewData getViewData ()
    {
        return this.viewData;
    }
}
