/*
 * This file is part of the openSCADA project
 * Copyright (C) 2006-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.ui.chart.viewer.input;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;

import org.eclipse.jface.resource.ResourceManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.LineAttributes;
import org.eclipse.swt.widgets.Display;
import org.openscada.chart.DataEntry;
import org.openscada.chart.Realm;
import org.openscada.chart.WritableSeries;
import org.openscada.chart.XAxis;
import org.openscada.chart.YAxis;
import org.openscada.chart.swt.ChartRenderer;
import org.openscada.chart.swt.render.AbstractLineRender;
import org.openscada.chart.swt.render.PositionYRuler;
import org.openscada.chart.swt.render.StepRenderer;
import org.openscada.core.Variant;
import org.openscada.core.data.SubscriptionState;
import org.openscada.da.client.DataItemValue;
import org.openscada.da.ui.connection.data.DataItemHolder;
import org.openscada.da.ui.connection.data.DataSourceListener;
import org.openscada.da.ui.connection.data.Item;
import org.openscada.ui.chart.Activator;
import org.openscada.ui.chart.viewer.ChartViewer;

public class ItemObserver extends LineInput implements DataSourceListener
{
    private static final String PROP_STATE = "state"; //$NON-NLS-1$

    private final Item item;

    private final WritableSeries valueSeries;

    private DataItemHolder dataItem;

    private DataItemValue lastValue;

    private DataEntry lastTickMarker;

    private final StepRenderer valueRenderer;

    private boolean selection;

    private final YAxis y;

    private final Collection<LevelRuler> levelRulers = new LinkedList<LevelRuler> ();

    private final ChartViewer viewer;

    private boolean disposed;

    private SubscriptionState state;

    private Date originalSelectedTimestamp;

    private static class LevelRuler
    {
        private final String prefix;

        private final PositionYRuler ruler;

        private final ChartRenderer manager;

        private final int alpha;

        public LevelRuler ( final ChartRenderer manager, final String prefix, final YAxis y, final int style, final int alpha, final float lineWidth )
        {
            this.prefix = prefix;

            this.manager = manager;

            this.alpha = alpha;

            this.ruler = new PositionYRuler ( y, style );
            this.ruler.setAlpha ( this.alpha );
            this.ruler.setLineAttributes ( new LineAttributes ( lineWidth ) );
            this.manager.addRenderer ( this.ruler, 200 );
        }

        public void dispose ()
        {
            this.manager.removeRenderer ( this.ruler );
        }

        public void update ( final DataItemValue value )
        {
            this.ruler.setVisible ( false );

            if ( value != null )
            {
                final Variant levelValue = value.getAttributes ().get ( this.prefix + ".preset" ); //$NON-NLS-1$
                if ( levelValue != null )
                {
                    this.ruler.setPosition ( levelValue.asDouble ( null ) );
                    this.ruler.setVisible ( true );
                }
                final boolean active = value.getAttributeAsBoolean ( this.prefix + ".active" ); //$NON-NLS-1$
                final boolean unsafe = value.getAttributeAsBoolean ( this.prefix + ".unsafe" ); //$NON-NLS-1$
                final boolean error = value.getAttributeAsBoolean ( this.prefix + ".error" ); //$NON-NLS-1$
                final boolean alarm = value.getAttributeAsBoolean ( this.prefix + ".alarm" ); //$NON-NLS-1$

                if ( !active )
                {
                    this.ruler.setColor ( Display.getDefault ().getSystemColor ( SWT.COLOR_GRAY ) );
                }
                else if ( unsafe )
                {
                    this.ruler.setColor ( Display.getDefault ().getSystemColor ( SWT.COLOR_MAGENTA ) );
                }
                else if ( error || alarm )
                {
                    this.ruler.setColor ( Display.getDefault ().getSystemColor ( SWT.COLOR_RED ) );
                }
                else
                {
                    this.ruler.setColor ( Display.getDefault ().getSystemColor ( SWT.COLOR_GREEN ) );
                }
            }
        }
    }

    public ItemObserver ( final ChartViewer viewer, final Item item, final Realm realm, final XAxis x, final YAxis y, final ResourceManager resourceManager )
    {
        super ( resourceManager );

        this.item = item;
        this.viewer = viewer;

        this.y = y;

        this.valueSeries = new WritableSeries ( realm, x, y );

        this.valueRenderer = new StepRenderer ( viewer.getChartRenderer (), this.valueSeries );
        viewer.getChartRenderer ().addRenderer ( this.valueRenderer, 0 );

        connect ();

        attachHover ( viewer, x );
    }

    @Override
    protected AbstractLineRender getLineRenderer ()
    {
        return this.valueRenderer;
    }

    /* (non-Javadoc)
     * @see org.openscada.ui.chart.view.ChartInput#setSelection(boolean)
     */
    @Override
    public void setSelection ( final boolean state )
    {
        if ( this.selection == state )
        {
            return;
        }
        this.selection = state;

        if ( this.selection )
        {
            this.levelRulers.add ( makeLevelRuler ( "org.openscada.da.level.ceil", SWT.NONE, 255, 2.0f ) ); //$NON-NLS-1$
            this.levelRulers.add ( makeLevelRuler ( "org.openscada.da.level.highhigh", SWT.TOP, 64, 1.0f ) ); //$NON-NLS-1$
            this.levelRulers.add ( makeLevelRuler ( "org.openscada.da.level.high", SWT.TOP, 32, 1.0f ) ); //$NON-NLS-1$
            this.levelRulers.add ( makeLevelRuler ( "org.openscada.da.level.low", SWT.BOTTOM, 32, 1.0f ) ); //$NON-NLS-1$
            this.levelRulers.add ( makeLevelRuler ( "org.openscada.da.level.lowlow", SWT.BOTTOM, 64, 1.0f ) ); //$NON-NLS-1$
            this.levelRulers.add ( makeLevelRuler ( "org.openscada.da.level.floor", SWT.NONE, 255, 2.0f ) ); //$NON-NLS-1$
            updateLevels ();
        }
        else
        {
            removeLevelRulers ();
        }
    }

    private void updateLevels ()
    {
        for ( final LevelRuler ruler : this.levelRulers )
        {
            ruler.update ( this.lastValue );
        }
    }

    private LevelRuler makeLevelRuler ( final String prefix, final int style, final int alpha, final float lineWidth )
    {
        final LevelRuler ruler = new LevelRuler ( this.viewer.getChartRenderer (), prefix, this.y, style, alpha, lineWidth );

        return ruler;
    }

    /* (non-Javadoc)
     * @see org.openscada.ui.chart.view.ChartInput#dispose()
     */
    @Override
    public void dispose ()
    {
        if ( this.disposed )
        {
            this.disposed = true;
        }

        removeLevelRulers ();

        this.viewer.removeInput ( this );
        this.viewer.getChartRenderer ().removeRenderer ( this.valueRenderer );
        this.valueRenderer.dispose ();
        disconnect ();
    }

    private void removeLevelRulers ()
    {
        for ( final LevelRuler ruler : this.levelRulers )
        {
            ruler.dispose ();
        }
        this.levelRulers.clear ();
    }

    public Item getItem ()
    {
        return this.item;
    }

    /* (non-Javadoc)
     * @see org.openscada.ui.chart.view.ChartInput#tick(long)
     */
    @Override
    public void tick ( final long now )
    {
        if ( this.lastTickMarker == null )
        {
            final DataEntry newMarker = makeEntry ( this.lastValue );
            if ( newMarker.getTimestamp () > now )
            {
                // don't add marker if the latest value is in the future
                return;
            }
            this.lastTickMarker = newMarker;
        }
        else
        {
            this.valueSeries.getData ().remove ( this.lastTickMarker );
        }
        this.lastTickMarker = new DataEntry ( now, this.lastTickMarker.getValue () );
        this.valueSeries.getData ().add ( this.lastTickMarker );
    }

    public void connect ()
    {
        this.dataItem = new DataItemHolder ( Activator.getDefault ().getBundle ().getBundleContext (), this.item, this );
    }

    public void disconnect ()
    {
        if ( this.dataItem != null )
        {
            this.dataItem.dispose ();
            this.dataItem = null;
        }
    }

    @Override
    public void updateData ( final DataItemValue value )
    {
        if ( !this.valueSeries.getRealm ().isDisposed () )
        {
            this.valueSeries.getRealm ().asyncExec ( new Runnable () {
                @Override
                public void run ()
                {
                    updateState ( value );
                    addNewValue ( value );
                }
            } );
        }
    }

    protected void updateState ( final DataItemValue value )
    {
        if ( value != null && this.state != value.getSubscriptionState () )
        {
            firePropertyChange ( PROP_STATE, this.state, this.state = value.getSubscriptionState () );
        }
    }

    private void addNewValue ( final DataItemValue value )
    {
        this.lastValue = value;

        if ( this.lastTickMarker != null )
        {
            this.valueSeries.getData ().remove ( this.lastTickMarker );
            this.lastTickMarker = null;
        }

        final DataEntry entry = makeEntry ( value );
        this.valueSeries.getData ().addAsLast ( entry );
        updateLevels ();

        setSelection ( this.originalSelectedTimestamp );
    }

    private DataEntry makeEntry ( final DataItemValue value )
    {
        if ( value == null || value.isError () || !value.isConnected () || value.getValue () == null )
        {
            return new DataEntry ( System.currentTimeMillis (), null );
        }
        else
        {
            final Calendar valueTimestamp = value.getTimestamp ();
            final long timestamp = valueTimestamp == null ? System.currentTimeMillis () : valueTimestamp.getTimeInMillis ();
            return new DataEntry ( timestamp, value.getValue ().asDouble ( null ) );
        }
    }

    @Override
    public String getState ()
    {
        final SubscriptionState state = this.state;
        return state == null ? null : state.name ();
    }

    @Override
    protected void setSelectedTimestamp ( final Date selectedTimestamp )
    {
        if ( selectedTimestamp == null )
        {
            return;
        }

        this.originalSelectedTimestamp = selectedTimestamp;

        if ( this.valueSeries == null || this.valueSeries.getData () == null )
        {
            return;
        }

        final DataEntry value = this.valueSeries.getData ().getEntries ().lower ( new DataEntry ( selectedTimestamp.getTime (), null ) );
        if ( value == null )
        {
            setSelectedValue ( null );
            super.setSelectedTimestamp ( null );
            setSelectedQuality ( Messages.ItemObserver_ZeroPercent );
        }
        else
        {
            setSelectedValue ( String.format ( Messages.ItemObserver_Format_Value, value.getValue () ) );
            setSelectedQuality ( value.getValue () == null ? Messages.ItemObserver_ZeroPercent : Messages.ItemObserver_100Percent );
            super.setSelectedTimestamp ( new Date ( value.getTimestamp () ) );
        }
    }

}