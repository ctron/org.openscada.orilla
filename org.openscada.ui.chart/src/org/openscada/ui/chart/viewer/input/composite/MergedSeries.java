/*
 * This file is part of the openSCADA project
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.ui.chart.viewer.input.composite;

import java.util.Map;

import org.openscada.chart.Realm;
import org.openscada.chart.WritableSeries;
import org.openscada.chart.XAxis;
import org.openscada.chart.YAxis;
import org.openscada.ui.chart.viewer.input.ArchiveInput;

public class MergedSeries extends WritableSeries
{

    private long startTimestamp;

    private long endTimestamp;

    private int width;

    private final Map<ArchiveInput, CompositeSource> sources;

    public MergedSeries ( final Map<ArchiveInput, CompositeSource> sources, final Realm realm, final XAxis xAxis, final YAxis yAxis )
    {
        super ( realm, xAxis, yAxis );
        this.sources = sources;
    }

    @Override
    public void setRequestWindow ( final long startTimestamp, final long endTimestamp )
    {
        if ( this.startTimestamp == startTimestamp && this.endTimestamp == endTimestamp )
        {
            return;
        }

        this.startTimestamp = startTimestamp;
        this.endTimestamp = endTimestamp;
        update ();
    }

    @Override
    public void setRequestWidth ( final int width )
    {
        if ( this.width == width )
        {
            return;
        }

        this.width = width;
        update ();
    }

    public void update ()
    {
        final MergeQualityData merger = new MergeQualityData ( this.sources.values (), this.startTimestamp, this.endTimestamp, this.width );
        merger.merge ();
        setData ( merger.getData () );
    }

}
