package org.openscada.ui.chart.viewer;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS
{
    private static final String BUNDLE_NAME = "org.openscada.ui.chart.viewer.messages"; //$NON-NLS-1$

    public static String ChartViewer_Title_NoItem;

    public static String ChartViewer_Title_NoSelection;

    public static String ChartViewer_Title_Selection;
    static
    {
        // initialize resource bundle
        NLS.initializeMessages ( BUNDLE_NAME, Messages.class );
    }

    private Messages ()
    {
    }
}
