/*
 * This file is part of the openSCADA project
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.ui.chart.viewer.profile;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.emf.databinding.EMFObservables;
import org.openscada.ui.chart.model.ChartModel.ChartPackage;
import org.openscada.ui.chart.model.ChartModel.Profile;
import org.openscada.ui.chart.viewer.ChartContext;
import org.openscada.ui.chart.viewer.controller.ControllerManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProfileEntry
{

    private final static Logger logger = LoggerFactory.getLogger ( ProfileEntry.class );

    private final ProfileManager manager;

    private final Profile profile;

    private boolean active;

    private final DataBindingContext ctx;

    private ControllerManager controllerManager;

    private final ChartContext chartContext;

    private Binding binding;

    public ProfileEntry ( final DataBindingContext ctx, final ProfileManager manager, final Profile profile, final ChartContext chartContext )
    {
        this.ctx = ctx;
        this.chartContext = chartContext;
        this.manager = manager;
        this.profile = profile;
    }

    public void activate ()
    {
        if ( this.active )
        {
            return;
        }

        this.active = true;

        logger.info ( "Activate profile: {}", this.profile.getId () ); //$NON-NLS-1$

        this.controllerManager = new ControllerManager ( this.ctx, this.ctx.getValidationRealm (), this.chartContext );
        this.binding = this.ctx.bindList ( this.controllerManager.getList (), EMFObservables.observeList ( this.profile, ChartPackage.Literals.PROFILE__CONTROLLERS ) );
    }

    public void deactivate ()
    {
        if ( !this.active )
        {
            return;
        }

        this.active = false;

        logger.info ( "Deactivate profile: {}", this.profile.getId () ); //$NON-NLS-1$

        if ( this.binding != null )
        {
            this.binding.dispose ();
            this.binding = null;
        }

        if ( this.controllerManager != null )
        {
            this.controllerManager.dispose ();
            this.controllerManager = null;
        }
    }

    protected void fireSelection ( final boolean selection )
    {
        if ( selection )
        {
            this.manager.setActiveProfile ( this.profile );
        }
        else
        {
            this.manager.setActiveProfile ( null );
        }
    }

    public void dispose ()
    {
    }

}
