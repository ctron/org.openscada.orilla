/*
 * This file is part of the openSCADA project
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.ui.chart.viewer.profile;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.openscada.ui.chart.model.ChartModel.Profile;
import org.openscada.ui.chart.viewer.ChartContext;

public class RadioProfileEntry extends ProfileEntry
{

    private final Button widget;

    public RadioProfileEntry ( final DataBindingContext ctx, final Composite parent, final ProfileManager profileManager, final Profile profile, final ChartContext chartContext )
    {
        super ( ctx, profileManager, profile, chartContext );

        this.widget = new Button ( parent, SWT.RADIO );
        this.widget.setText ( profile.getLabel () );

        this.widget.addSelectionListener ( new SelectionAdapter () {
            @Override
            public void widgetSelected ( final SelectionEvent e )
            {
                fireSelection ( RadioProfileEntry.this.widget.getSelection () );
            };
        } );
    }

    @Override
    public void activate ()
    {
        this.widget.setSelection ( true );

        super.activate ();
    }

}
