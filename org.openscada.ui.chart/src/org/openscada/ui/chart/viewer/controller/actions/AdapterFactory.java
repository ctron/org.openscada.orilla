/*
 * This file is part of the openSCADA project
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.ui.chart.viewer.controller.actions;

import org.eclipse.core.runtime.IAdapterFactory;
import org.openscada.ui.chart.model.ChartModel.ScaleAction;
import org.openscada.ui.chart.model.ChartModel.TimeNowAction;
import org.openscada.ui.chart.model.ChartModel.TimeShiftAction;
import org.openscada.ui.chart.viewer.controller.ChartControllerFactory;

public class AdapterFactory implements IAdapterFactory
{

    @SuppressWarnings ( "rawtypes" )
    private static final Class[] CLASSES = new Class[] { ChartControllerFactory.class };

    @SuppressWarnings ( "rawtypes" )
    @Override
    public Object getAdapter ( final Object adaptableObject, final Class adapterType )
    {
        if ( adaptableObject instanceof TimeShiftAction && adapterType == ChartControllerFactory.class )
        {
            return new TimeShiftActionFactory ();
        }
        if ( adaptableObject instanceof TimeNowAction && adapterType == ChartControllerFactory.class )
        {
            return new TimeNowActionFactory ();
        }
        if ( adaptableObject instanceof ScaleAction && adapterType == ChartControllerFactory.class )
        {
            return new ScaleActionFactory ();
        }
        return null;
    }

    @SuppressWarnings ( "rawtypes" )
    @Override
    public Class[] getAdapterList ()
    {
        return CLASSES;
    }

}
