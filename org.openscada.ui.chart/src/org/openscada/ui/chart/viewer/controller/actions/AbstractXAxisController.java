/*
 * This file is part of the openSCADA project
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.ui.chart.viewer.controller.actions;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.list.WritableList;
import org.eclipse.emf.databinding.EMFObservables;
import org.openscada.ui.chart.model.ChartModel.ChartPackage;
import org.openscada.ui.chart.model.ChartModel.XAxis;
import org.openscada.ui.chart.model.ChartModel.XAxisController;
import org.openscada.ui.chart.viewer.AbstractObserver;
import org.openscada.ui.chart.viewer.AxisLocator;
import org.openscada.ui.chart.viewer.ChartContext;
import org.openscada.ui.chart.viewer.XAxisViewer;

public class AbstractXAxisController extends AbstractObserver
{
    private final AxisLocator<XAxis, XAxisViewer> xLocator;

    private final WritableList list;

    public AbstractXAxisController ( final DataBindingContext ctx, final ChartContext chartContext, final XAxisController controller )
    {
        this.list = new WritableList ( ctx.getValidationRealm () );

        this.xLocator = chartContext.getxAxisLocator ();
        addBinding ( ctx.bindList ( this.list, EMFObservables.observeList ( controller, ChartPackage.Literals.XAXIS_CONTROLLER__AXIS ) ) );
    }

    protected boolean isAllAxisIfEmpty ()
    {
        return true;
    }

    protected List<XAxisViewer> getCurrentViewers ()
    {
        final List<XAxisViewer> result = new LinkedList<XAxisViewer> ();

        if ( this.list.isEmpty () && isAllAxisIfEmpty () )
        {
            return this.xLocator.getAll ();
        }

        for ( final Object o : this.list )
        {
            if ( ! ( o instanceof XAxis ) )
            {
                continue;
            }
            final XAxisViewer x = this.xLocator.findAxis ( (XAxis)o );
            result.add ( x );
        }

        return result;
    }
}
