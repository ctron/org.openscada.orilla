/*
 * This file is part of the openSCADA project
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.ui.chart.viewer.controller;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.list.IListChangeListener;
import org.eclipse.core.databinding.observable.list.ListChangeEvent;
import org.eclipse.core.databinding.observable.list.ListDiff;
import org.eclipse.core.databinding.observable.list.ListDiffVisitor;
import org.eclipse.core.databinding.observable.list.WritableList;
import org.openscada.ui.chart.model.ChartModel.Controller;
import org.openscada.ui.chart.viewer.ChartContext;
import org.openscada.ui.databinding.AdapterHelper;

public class ControllerManager
{
    private final WritableList list;

    private final DataBindingContext ctx;

    private final ChartContext chartContext;

    private final Realm realm;

    public ControllerManager ( final DataBindingContext ctx, final Realm realm, final ChartContext chartContext )
    {
        this.ctx = ctx;
        this.realm = realm;
        this.chartContext = chartContext;
        this.list = new WritableList ();
        this.list.addListChangeListener ( new IListChangeListener () {

            @Override
            public void handleListChange ( final ListChangeEvent event )
            {
                handleChange ( event.diff );
            }
        } );
    }

    protected void handleChange ( final ListDiff diff )
    {
        diff.accept ( new ListDiffVisitor () {

            @Override
            public void handleRemove ( final int index, final Object element )
            {
                ControllerManager.this.handleRemove ( (Controller)element );
            }

            @Override
            public void handleAdd ( final int index, final Object element )
            {
                ControllerManager.this.handleAdd ( (Controller)element );
            }
        } );
    }

    private final Map<Controller, ChartController> controllerMap = new HashMap<Controller, ChartController> ();

    protected void handleAdd ( final Controller controller )
    {
        handleRemove ( controller );
        final ChartController chartController = createController ( controller );
        if ( chartController != null )
        {
            this.controllerMap.put ( controller, chartController );
        }
    }

    protected void handleRemove ( final Controller controller )
    {
        final ChartController chartController = this.controllerMap.get ( controller );
        if ( chartController != null )
        {
            chartController.dispose ();
        }
    }

    private ChartControllerFactory createFactory ( final Controller controller )
    {
        final ChartControllerFactory factory = AdapterHelper.adapt ( controller, ChartControllerFactory.class );
        if ( factory != null )
        {
            return factory;
        }
        return null;
    }

    private ChartController createController ( final Controller controller )
    {
        final ChartControllerFactory factory = createFactory ( controller );
        if ( factory == null )
        {
            return null;
        }

        return factory.create ( this, controller, this.chartContext );
    }

    public WritableList getList ()
    {
        return this.list;
    }

    public void dispose ()
    {
        for ( final ChartController chartController : this.controllerMap.values () )
        {
            chartController.dispose ();
        }
        this.controllerMap.clear ();

        this.list.dispose ();
    }

    public DataBindingContext getContext ()
    {
        return this.ctx;
    }

    public Realm getRealm ()
    {
        return this.realm;
    }

    public ChartContext getChartContext ()
    {
        return this.chartContext;
    }
}
