/*
 * This file is part of the openSCADA project
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.ui.chart.viewer.controller.actions;

import java.util.concurrent.TimeUnit;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.beans.PojoObservables;
import org.eclipse.emf.databinding.EMFObservables;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.openscada.ui.chart.model.ChartModel.ChartPackage;
import org.openscada.ui.chart.model.ChartModel.ScaleAction;
import org.openscada.ui.chart.viewer.ChartContext;
import org.openscada.ui.chart.viewer.XAxisViewer;
import org.openscada.ui.chart.viewer.controller.ChartController;
import org.openscada.ui.chart.viewer.controller.ControllerManager;

public class ScaleActionController extends AbstractXAxisController implements ChartController
{

    private Button button;

    private long milliseconds;

    public ScaleActionController ( final ControllerManager controllerManager, final ChartContext chartContext, final ScaleAction controller )
    {
        super ( controllerManager.getContext (), chartContext, controller );

        final DataBindingContext ctx = controllerManager.getContext ();

        final Composite space = chartContext.getExtensionSpaceProvider ().getExtensionSpace ();
        if ( space != null )
        {
            this.button = new Button ( space, SWT.PUSH );
            this.button.addSelectionListener ( new SelectionAdapter () {
                @Override
                public void widgetSelected ( final SelectionEvent e )
                {
                    action ();
                };
            } );
            addBinding ( ctx.bindValue ( PojoObservables.observeValue ( this, "milliseconds" ), EMFObservables.observeValue ( controller, ChartPackage.Literals.SCALE_ACTION__TIMESPAN ) ) ); //$NON-NLS-1$
            addBinding ( ctx.bindValue ( SWTObservables.observeText ( this.button ), EMFObservables.observeValue ( controller, ChartPackage.Literals.SCALE_ACTION__LABEL ) ) );
            addBinding ( ctx.bindValue ( SWTObservables.observeTooltipText ( this.button ), EMFObservables.observeValue ( controller, ChartPackage.Literals.SCALE_ACTION__DESCRIPTION ) ) );
            space.layout ();
        }
        else
        {
            this.button = null;
        }
    }

    protected void action ()
    {
        for ( final XAxisViewer viewer : getCurrentViewers () )
        {
            viewer.getAxis ().setByTimespan ( this.milliseconds, TimeUnit.MILLISECONDS );
        }
    }

    @Override
    public void dispose ()
    {
        if ( this.button != null )
        {
            this.button.dispose ();
        }
        super.dispose ();
    }

    public void setMilliseconds ( final long milliseconds )
    {
        this.milliseconds = milliseconds;
    }

    public long getMilliseconds ()
    {
        return this.milliseconds;
    }

}
