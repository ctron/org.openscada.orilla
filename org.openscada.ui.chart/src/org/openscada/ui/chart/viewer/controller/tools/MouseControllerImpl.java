/*
 * This file is part of the openSCADA project
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.ui.chart.viewer.controller.tools;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.beans.PojoObservables;
import org.eclipse.emf.databinding.EMFObservables;
import org.openscada.chart.swt.controller.MouseDragZoomer;
import org.openscada.chart.swt.controller.MouseTransformer;
import org.openscada.chart.swt.controller.MouseWheelZoomer;
import org.openscada.ui.chart.model.ChartModel.Chart;
import org.openscada.ui.chart.model.ChartModel.ChartPackage;
import org.openscada.ui.chart.model.ChartModel.MouseController;
import org.openscada.ui.chart.model.ChartModel.XAxis;
import org.openscada.ui.chart.model.ChartModel.YAxis;
import org.openscada.ui.chart.viewer.AbstractObserver;
import org.openscada.ui.chart.viewer.ChartContext;
import org.openscada.ui.chart.viewer.XAxisViewer;
import org.openscada.ui.chart.viewer.YAxisViewer;
import org.openscada.ui.chart.viewer.controller.ChartController;
import org.openscada.ui.chart.viewer.controller.ControllerManager;

public class MouseControllerImpl extends AbstractObserver implements ChartController
{

    private final ChartContext chartContext;

    private MouseTransformer mouseTransformer;

    private MouseDragZoomer mouseDragZoomer;

    private MouseWheelZoomer mouseWheelZoomer;

    private XAxisViewer selectedXAxis;

    private YAxisViewer selectedYAxis;

    private YAxis selectedYAxisElement;

    private XAxis selectedXAxisElement;

    public MouseControllerImpl ( final ControllerManager controllerManager, final ChartContext chartContext, final MouseController controller )
    {
        this.chartContext = chartContext;

        final Chart chart = chartContext.getChart ();

        final DataBindingContext ctx = controllerManager.getContext ();
        ctx.bindValue ( PojoObservables.observeValue ( this, "selectedXAxis" ), EMFObservables.observeValue ( chart, ChartPackage.Literals.CHART__SELECTED_XAXIS ) ); //$NON-NLS-1$
        ctx.bindValue ( PojoObservables.observeValue ( this, "selectedYAxis" ), EMFObservables.observeValue ( chart, ChartPackage.Literals.CHART__SELECTED_YAXIS ) ); //$NON-NLS-1$
    }

    protected void activate ( final org.openscada.chart.XAxis x, final org.openscada.chart.YAxis y )
    {
        this.mouseTransformer = new MouseTransformer ( this.chartContext.getChartRenderer (), x, y );
        this.mouseDragZoomer = new MouseDragZoomer ( this.chartContext.getChartRenderer (), x, y );
        this.mouseWheelZoomer = new MouseWheelZoomer ( this.chartContext.getChartRenderer (), x, y );
    }

    protected void deactivate ()
    {
        if ( this.mouseTransformer != null )
        {
            this.mouseTransformer.dispose ();
            this.mouseTransformer = null;
        }

        if ( this.mouseDragZoomer != null )
        {
            this.mouseDragZoomer.dispose ();
            this.mouseDragZoomer = null;
        }

        if ( this.mouseWheelZoomer != null )
        {
            this.mouseWheelZoomer.dispose ();
            this.mouseWheelZoomer = null;
        }
    }

    protected void updateState ()
    {
        final org.openscada.chart.XAxis x;
        final org.openscada.chart.YAxis y;

        x = getSelectedXAxisViewer ();
        y = getSelectedYAxisViewer ();

        // update mouse controllers

        if ( this.mouseTransformer != null )
        {
            this.mouseTransformer.dispose ();
            this.mouseTransformer = null;
        }
        if ( this.mouseDragZoomer != null )
        {
            this.mouseDragZoomer.dispose ();
            this.mouseDragZoomer = null;
        }
        if ( this.mouseWheelZoomer != null )
        {
            this.mouseWheelZoomer.dispose ();
            this.mouseWheelZoomer = null;
        }
        if ( x != null && y != null )
        {
            activate ( x, y );
        }
        else
        {
            deactivate ();
        }
    }

    private org.openscada.chart.YAxis getSelectedYAxisViewer ()
    {
        return this.selectedYAxis != null ? this.selectedYAxis.getAxis () : null;
    }

    private org.openscada.chart.XAxis getSelectedXAxisViewer ()
    {
        return this.selectedXAxis != null ? this.selectedXAxis.getAxis () : null;
    }

    public XAxis getSelectedXAxis ()
    {
        return this.selectedXAxisElement;
    }

    public YAxis getSelectedYAxis ()
    {
        return this.selectedYAxisElement;
    }

    public void setSelectedXAxis ( final XAxis axis )
    {
        final XAxisViewer newSelection = this.chartContext.getxAxisLocator ().findAxis ( axis );
        if ( this.selectedXAxis == newSelection )
        {
            return;
        }
        this.selectedXAxis = newSelection;
        this.selectedXAxisElement = axis;
        updateState ();
    }

    public void setSelectedYAxis ( final YAxis axis )
    {
        final YAxisViewer newSelection = this.chartContext.getyAxisLocator ().findAxis ( axis );
        if ( this.selectedYAxis == newSelection )
        {
            return;
        }
        this.selectedYAxis = newSelection;
        this.selectedYAxisElement = axis;
        updateState ();
    }

    @Override
    public void dispose ()
    {
        deactivate ();
        super.dispose ();
    }

}
