/*
 * This file is part of the openSCADA project
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.ui.chart.viewer;

import java.util.LinkedList;
import java.util.List;

public class SimpleAxisLocator<Key, Value> implements AxisLocator<Key, Value>
{
    private final AbstractAxisManager<Key, Value> first;

    private final AbstractAxisManager<Key, Value> second;

    public SimpleAxisLocator ( final AbstractAxisManager<Key, Value> first, final AbstractAxisManager<Key, Value> second )
    {
        this.first = first;
        this.second = second;
    }

    @Override
    public List<Value> getAll ()
    {
        final List<Value> result = new LinkedList<Value> ();

        result.addAll ( this.first.getAll () );
        result.addAll ( this.second.getAll () );

        return result;
    }

    @Override
    public Value findAxis ( final Key key )
    {
        final Value result = this.first.getAxis ( key );
        if ( result != null )
        {
            return result;
        }
        return this.second.getAxis ( key );
    }

}
