package org.openscada.ui.chart.selector;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS
{
    private static final String BUNDLE_NAME = "org.openscada.ui.chart.selector.messages"; //$NON-NLS-1$

    public static String ChartConfigurationInputSelector_Channels_Label;

    public static String ChartInputSelector_Channels_Label;
    static
    {
        // initialize resource bundle
        NLS.initializeMessages ( BUNDLE_NAME, Messages.class );
    }

    private Messages ()
    {
    }
}
