/*
 * This file is part of the OpenSCADA project
 * 
 * Copyright (C) 2006-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * OpenSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.core.ui.styles;

import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.openscada.core.ui.styles.generator.PreferenceSelectorStyleGenerator;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin
{

    // The plug-in ID
    public static final String PLUGIN_ID = "org.openscada.core.ui.styles"; //$NON-NLS-1$

    private PreferenceSelectorStyleGenerator defaultStyleGenerator;

    // The shared instance
    private static Activator plugin;

    /**
     * The constructor
     */
    public Activator ()
    {
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
     */
    @Override
    public void start ( final BundleContext context ) throws Exception
    {
        super.start ( context );
        plugin = this;
        this.defaultStyleGenerator = new PreferenceSelectorStyleGenerator ( getPreferenceStore () );
    }

    @Override
    protected void initializeImageRegistry ( final ImageRegistry reg )
    {
        super.initializeImageRegistry ( reg );

        reg.put ( ImageConstants.IMG_MANUAL, imageDescriptorFromPlugin ( PLUGIN_ID, "icons/manual.png" ) ); //$NON-NLS-1$
        reg.put ( ImageConstants.IMG_BLOCK, imageDescriptorFromPlugin ( PLUGIN_ID, "icons/block.png" ) ); //$NON-NLS-1$
        reg.put ( ImageConstants.IMG_DISCONNECTED, imageDescriptorFromPlugin ( PLUGIN_ID, "icons/disconnected.png" ) ); //$NON-NLS-1$
        reg.put ( ImageConstants.IMG_ERROR, imageDescriptorFromPlugin ( PLUGIN_ID, "icons/error.png" ) ); //$NON-NLS-1$
        reg.put ( ImageConstants.IMG_ALARM, imageDescriptorFromPlugin ( PLUGIN_ID, "icons/alarm.png" ) ); //$NON-NLS-1$
        reg.put ( ImageConstants.IMG_WARNING, imageDescriptorFromPlugin ( PLUGIN_ID, "icons/warning.png" ) ); //$NON-NLS-1$
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
     */
    @Override
    public void stop ( final BundleContext context ) throws Exception
    {
        this.defaultStyleGenerator.dispose ();
        plugin = null;
        super.stop ( context );
    }

    /**
     * Returns the shared instance
     * 
     * @return the shared instance
     */
    public static Activator getDefault ()
    {
        return plugin;
    }

    public static StyleGenerator getDefaultStyleGenerator ()
    {
        return Activator.plugin.defaultStyleGenerator;
    }
}
