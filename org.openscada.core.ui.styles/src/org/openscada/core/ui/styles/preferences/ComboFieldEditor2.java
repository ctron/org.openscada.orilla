/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.core.ui.styles.preferences;

import org.eclipse.core.runtime.SafeRunner;
import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.util.SafeRunnable;
import org.eclipse.swt.widgets.Composite;

/**
 * This is a dirty hack
 */
class ComboFieldEditor2 extends ComboFieldEditor
{

    public ComboFieldEditor2 ( final String name, final String labelText, final String[][] entryNamesAndValues, final Composite parent )
    {
        super ( name, labelText, entryNamesAndValues, parent );
    }

    interface Callback
    {
        void valueChange ( Object value );
    }

    private Callback callback;

    public void setCallback ( final Callback callback )
    {
        this.callback = callback;
    }

    @Override
    protected void fireValueChanged ( final String property, final Object oldValue, final Object newValue )
    {
        if ( VALUE.equals ( property ) )
        {
            if ( this.callback != null )
            {
                SafeRunner.run ( new SafeRunnable () {

                    @Override
                    public void run () throws Exception
                    {
                        ComboFieldEditor2.this.callback.valueChange ( newValue );
                    }
                } );
            }
        }
        super.fireValueChanged ( property, oldValue, newValue );
    }

}
