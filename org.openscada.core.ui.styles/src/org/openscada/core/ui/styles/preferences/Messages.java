package org.openscada.core.ui.styles.preferences;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS
{
    private static final String BUNDLE_NAME = "org.openscada.core.ui.styles.preferences.messages"; //$NON-NLS-1$

    public static String StylePreferencePage_Description;

    public static String StylePreferencePage_Description_Label;

    public static String StylePreferencePage_Label;
    static
    {
        // initialize resource bundle
        NLS.initializeMessages ( BUNDLE_NAME, Messages.class );
    }

    private Messages ()
    {
    }
}
