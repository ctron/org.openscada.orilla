/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.core.ui.styles;

import org.eclipse.jface.resource.ColorDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.ResourceManager;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.graphics.Color;

public class ColorUpdater implements IPropertyChangeListener
{

    private final String name;

    private final ResourceManager manager;

    private final ColorDescriptor defaultColor;

    private ColorDescriptor activeColor;

    private Color color;

    public ColorUpdater ( final String name, final ResourceManager manager, final ColorDescriptor defaultColor )
    {
        this.name = name;
        this.manager = manager;
        this.defaultColor = defaultColor;
        JFaceResources.getColorRegistry ().addListener ( this );
        refresh ();
    }

    public void dispose ()
    {
        JFaceResources.getColorRegistry ().removeListener ( this );
    }

    @Override
    public void propertyChange ( final PropertyChangeEvent event )
    {
        if ( this.name != null && this.name.equals ( event.getProperty () ) )
        {
            refresh ();
        }
    }

    private void refresh ()
    {
        if ( this.color != null )
        {
            // we may not dispose, the manager has to do that
            this.color = null;
        }
        if ( this.activeColor != null )
        {
            this.manager.destroyColor ( this.activeColor );
            this.activeColor = null;
        }

        this.activeColor = JFaceResources.getColorRegistry ().getColorDescriptor ( this.name, this.defaultColor );
        if ( this.activeColor != null )
        {
            this.color = this.manager.createColor ( this.activeColor );
        }
    }

    public Color getColor ()
    {
        return this.color;
    }
}
