/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.core.ui.styles.generator;

import org.openscada.core.ui.styles.StateInformation;
import org.openscada.core.ui.styles.StyleGenerator;
import org.openscada.core.ui.styles.StyleHandler.Style;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A style generator which forwards requests to another style generator.
 * <p>
 * Also will it listen to {@link GeneratorListener} events and fire them as
 * events.
 * </p>
 * <p>
 * Note that calling dispose on this instance will NOT dispose the currently
 * referenced style generator.
 * </p>
 * 
 * @author Jens Reimann
 */
public class DelegatingStyleGenerator extends AbstractStyleGenerator
{

    private final static Logger logger = LoggerFactory.getLogger ( DelegatingStyleGenerator.class );

    private StyleGenerator generator;

    private final GeneratorListener listener = new GeneratorListener () {

        @Override
        public void configurationChanged ()
        {
            handleConfigurationChanged ();
        }
    };

    protected void setStyleGenerator ( final StyleGenerator generator )
    {
        logger.debug ( "Set style generator to {}", generator ); //$NON-NLS-1$

        if ( this.generator != null )
        {
            this.generator.removeListener ( this.listener );
            this.generator = null;
        }

        this.generator = generator;

        if ( this.generator != null )
        {
            this.generator.addListener ( this.listener );
        }

        // the generator itself has changed, fire change
        fireConfigurationChanged ();
    }

    protected void handleConfigurationChanged ()
    {
        // forward change event
        fireConfigurationChanged ();
    }

    @Override
    public Style generateStyle ( final StateInformation state )
    {
        if ( this.generator == null )
        {
            return null;
        }
        else
        {
            return this.generator.generateStyle ( state );
        }
    }

    @Override
    public void dispose ()
    {
    }
}
