package org.openscada.core.ui.styles.generator;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS
{
    private static final String BUNDLE_NAME = "org.openscada.core.ui.styles.generator.messages"; //$NON-NLS-1$

    public static String PreferenceSelectorStyleGenerator_2;
    static
    {
        // initialize resource bundle
        NLS.initializeMessages ( BUNDLE_NAME, Messages.class );
    }

    private Messages ()
    {
    }
}
