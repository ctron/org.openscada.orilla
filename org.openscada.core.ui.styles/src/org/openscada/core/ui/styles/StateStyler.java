/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.core.ui.styles;

import org.openscada.core.ui.styles.StyleGenerator.GeneratorListener;

/**
 * The state styler combines all parts of the state to visual style
 * transformation
 * <p>
 * Instead of choosing a {@link StyleGenerator} and forwarding the resulting
 * styles to a {@link StyleHandler} everything is done in one place.
 * </p>
 * 
 * @author Jens Reimann
 */
public class StateStyler
{
    private final StyleGenerator generator;

    private StateInformation currentState;

    private final StyleHandler handler;

    private final GeneratorListener listener = new GeneratorListener () {

        @Override
        public void configurationChanged ()
        {
            refresh ();
        }
    };

    public StateStyler ( final StyleHandler handler )
    {
        this.generator = Activator.getDefaultStyleGenerator ();
        this.handler = handler;

        this.generator.addListener ( this.listener );
    }

    protected void refresh ()
    {
        this.handler.setStyle ( this.generator.generateStyle ( this.currentState ) );
    }

    public void style ( final StateInformation state )
    {
        this.currentState = state;
        this.handler.setStyle ( this.generator.generateStyle ( state ) );
    }

    public void dispose ()
    {
        this.generator.removeListener ( this.listener );
        this.generator.dispose ();
    }
}
