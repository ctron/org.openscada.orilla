/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.core.ui.styles;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;

public class StyleGeneratorInformation
{
    private static final String EXTP_STYLE_GENERATOR = "org.openscada.core.ui.styles.styleGenerator"; //$NON-NLS-1$

    private static final String ELE_STYLE_GENERATOR = "styleGenerator"; //$NON-NLS-1$

    private final String id;

    private final String name;

    private final String description;

    public StyleGeneratorInformation ( final String id, final String name, final String description )
    {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public String getDescription ()
    {
        return this.description;
    }

    public String getId ()
    {
        return this.id;
    }

    public String getName ()
    {
        return this.name;
    }

    public static List<StyleGeneratorInformation> list ()
    {
        final List<StyleGeneratorInformation> result = new LinkedList<StyleGeneratorInformation> ();

        for ( final IConfigurationElement ele : Platform.getExtensionRegistry ().getConfigurationElementsFor ( EXTP_STYLE_GENERATOR ) )
        {
            if ( !ELE_STYLE_GENERATOR.equals ( ele.getName () ) )
            {
                continue;
            }

            final String id = ele.getAttribute ( "id" ); //$NON-NLS-1$
            final String name = ele.getAttribute ( "name" ); //$NON-NLS-1$
            final String description = getText ( ele.getChildren ( "description" ) ); //$NON-NLS-1$

            result.add ( new StyleGeneratorInformation ( id, name, description ) );
        }

        return result;
    }

    private static String getText ( final IConfigurationElement[] children )
    {
        if ( children == null )
        {
            return null;
        }
        for ( final IConfigurationElement ele : children )
        {
            return ele.getValue ();
        }
        return null;
    }

    public static IConfigurationElement getConfiguration ( final String generatorId )
    {
        if ( generatorId == null )
        {
            return null;
        }

        for ( final IConfigurationElement ele : Platform.getExtensionRegistry ().getConfigurationElementsFor ( EXTP_STYLE_GENERATOR ) )
        {
            if ( !ELE_STYLE_GENERATOR.equals ( ele.getName () ) )
            {
                continue;
            }

            final String id = ele.getAttribute ( "id" ); //$NON-NLS-1$
            if ( id == null )
            {
                continue;
            }
            if ( id.equals ( generatorId ) )
            {
                return ele;
            }
        }

        return null;
    }
}
