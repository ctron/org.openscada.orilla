/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.sec.ui;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.swt.widgets.Composite;

public interface CallbackWidgetFactory
{
    /**
     * Create controls inside a 2 column grid
     * 
     * @param dbc
     *            the data binding context
     * @param composite
     *            the 2 column grid composite
     */
    public void createGridWidgets ( DataBindingContext dbc, Composite composite );

    /**
     * Complete the data entry
     * <p>
     * This can be used as the last point where the callback data can be updated
     * before it is sent to the requester
     * </p>
     */
    public void complete ();

    public boolean tryInstantComplete ();
}
