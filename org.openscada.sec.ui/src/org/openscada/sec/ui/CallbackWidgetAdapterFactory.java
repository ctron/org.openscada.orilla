/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.sec.ui;

import org.eclipse.core.runtime.IAdapterFactory;
import org.openscada.sec.callback.PasswordCallback;
import org.openscada.sec.callback.TextCallback;
import org.openscada.sec.callback.UserNameCallback;

public final class CallbackWidgetAdapterFactory implements IAdapterFactory
{
    @SuppressWarnings ( "rawtypes" )
    @Override
    public Class[] getAdapterList ()
    {
        return new Class[] { CallbackWidgetFactory.class };
    }

    @SuppressWarnings ( "rawtypes" )
    @Override
    public CallbackWidgetFactory getAdapter ( final Object adaptableObject, final Class adapterType )
    {
        if ( adaptableObject instanceof TextCallback )
        {
            return new TextWidgetFactory ( (TextCallback)adaptableObject );
        }
        else if ( adaptableObject instanceof UserNameCallback )
        {
            return new TextWidgetFactory ( (TextCallback)adaptableObject );
        }
        else if ( adaptableObject instanceof PasswordCallback )
        {
            return new PasswordWidgetFactory ( (PasswordCallback)adaptableObject );
        }
        else
        {
            return null;
        }
    }
}