/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.sec.ui;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.openscada.sec.callback.LabelCallback;

public abstract class AbstractLabelCallbackFactory implements CallbackWidgetFactory
{
    private final LabelCallback labelCallback;

    public AbstractLabelCallbackFactory ( final LabelCallback labelCallback )
    {
        this.labelCallback = labelCallback;
    }

    @Override
    public void createGridWidgets ( final DataBindingContext dbc, final Composite composite )
    {
        final Label label = new Label ( composite, SWT.NONE );
        label.setText ( String.format ( "%s: ", this.labelCallback.getLabel () ) );

        createInput ( dbc, label, composite );
    }

    @Override
    public void complete ()
    {
    }

    @Override
    public boolean tryInstantComplete ()
    {
        return false;
    }

    protected abstract void createInput ( DataBindingContext dbc, final Label label, final Composite composite );
}
