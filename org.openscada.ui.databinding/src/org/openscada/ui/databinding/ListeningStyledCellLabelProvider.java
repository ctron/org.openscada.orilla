/*
 * This file is part of the OpenSCADA project
 * 
 * Copyright (C) 2006-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * OpenSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.ui.databinding;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.core.databinding.observable.IObservable;
import org.eclipse.core.databinding.observable.set.IObservableSet;
import org.eclipse.core.databinding.observable.set.ISetChangeListener;
import org.eclipse.core.databinding.observable.set.SetChangeEvent;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.IValueChangeListener;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.core.databinding.property.value.IValueProperty;
import org.eclipse.jface.viewers.LabelProviderChangedEvent;
import org.eclipse.jface.viewers.StyledCellLabelProvider;

public abstract class ListeningStyledCellLabelProvider extends StyledCellLabelProvider
{

    private final ISetChangeListener listener = new ISetChangeListener () {
        @Override
        public void handleSetChange ( final SetChangeEvent event )
        {
            for ( final Iterator<?> it = event.diff.getAdditions ().iterator (); it.hasNext (); )
            {
                addListenerTo ( it.next () );
            }
            for ( final Iterator<?> it = event.diff.getRemovals ().iterator (); it.hasNext (); )
            {
                processRemove ( it.next () );
            }
        }
    };

    private final IObservableSet items;

    public ListeningStyledCellLabelProvider ( final IObservableSet itemsThatNeedLabels )
    {
        this.items = itemsThatNeedLabels;
        this.items.addSetChangeListener ( this.listener );
        for ( final Iterator<?> it = this.items.iterator (); it.hasNext (); )
        {
            addListenerTo ( it.next () );
        }
    }

    private final Map<Object, IObservable> observables = new HashMap<Object, IObservable> ();

    /**
     * Listen to changed of the value property of an element
     * <p>
     * This will start listening to value change events and trigger a refresh of
     * the label provider. It will not actually take the value for displaying.
     * </p>
     * <p>
     * Elements that are listened to will automatically removed
     * </p>
     * 
     * @since 1.1
     * @param element
     *            the element to listen to
     * @param property
     *            the property to listen to
     */
    protected void listenTo ( final Object element, final IValueProperty property )
    {
        final IObservableValue obs = property.observe ( element );
        this.observables.put ( element, obs );
        obs.addValueChangeListener ( new IValueChangeListener () {

            @Override
            public void handleValueChange ( final ValueChangeEvent event )
            {
                fireLabelProviderChanged ( new LabelProviderChangedEvent ( ListeningStyledCellLabelProvider.this, element ) );
            }
        } );
    }

    protected void processRemove ( final Object element )
    {
        final IObservable obs = this.observables.remove ( element );
        if ( obs != null )
        {
            obs.dispose ();
        }
        else
        {
            removeListenerFrom ( element );
        }
    }

    @Override
    public void dispose ()
    {
        for ( final IObservable obs : this.observables.values () )
        {
            obs.dispose ();
        }
        this.observables.clear ();
        super.dispose ();
    }

    /**
     * @param element
     */
    protected abstract void removeListenerFrom ( Object element );

    /**
     * @param element
     */
    protected abstract void addListenerTo ( Object element );

}
