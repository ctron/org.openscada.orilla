/*
 * This file is part of the OpenSCADA project
 * Copyright (C) 2006-2010 TH4 SYSTEMS GmbH (http://th4-systems.com)
 *
 * OpenSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.ui.databinding;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IAdapterManager;
import org.eclipse.core.runtime.Platform;

public class AdapterHelper
{
    /**
     * Adapt an object to the requested target class if possible
     * <p>
     * The following order is tried:
     * <ul>
     * <li>via casting</li>
     * <li>via {@link IAdaptable}</li>
     * <li>via {@link IAdapterManager#getAdapter(Object, Class)}</li>
     * </p>
     * 
     * @param target
     *            the object to convert
     * @param adapterClass
     *            the target class
     * @return an instance of the target class or <code>null</code> if the
     *         object cannot be adapted to the target class
     */
    public static <T> T adapt ( final Object target, final Class<T> adapterClass )
    {
        return adapt ( target, adapterClass, false );
    }

    /**
     * Adapt an object to the requested target class if possible
     * <p>
     * The following order is tried:
     * <ul>
     * <li>via casting</li>
     * <li>via {@link IAdaptable}</li>
     * <li>via {@link IAdapterManager#getAdapter(Object, Class)} if the
     * parameter
     * <q>load</q> is <code>false</code></li>
     * <li>via {@link IAdapterManager#loadAdapter(Object, String)} if the
     * parameter
     * <q>load</q> is <code>true</code></li>
     * </p>
     * 
     * @since 1.1
     * @param target
     *            the object to convert
     * @param adapterClass
     *            the target class
     * @param load
     *            a flag allowing to control if the adapter manager should also
     *            try loading bundles. See
     *            {@link IAdapterManager#loadAdapter(Object, String)}
     * @return an instance of the target class or <code>null</code> if the
     *         object cannot be adapted to the target class
     */
    @SuppressWarnings ( "unchecked" )
    public static <T> T adapt ( final Object target, final Class<T> adapterClass, final boolean load )
    {
        if ( target == null )
        {
            return null;
        }

        if ( adapterClass.isAssignableFrom ( target.getClass () ) )
        {
            return adapterClass.cast ( target );
        }

        if ( target instanceof IAdaptable )
        {
            return (T) ( (IAdaptable)target ).getAdapter ( adapterClass );
        }

        if ( load )
        {
            return (T)Platform.getAdapterManager ().loadAdapter ( target, adapterClass.getName () );
        }
        else
        {
            return (T)Platform.getAdapterManager ().getAdapter ( target, adapterClass );
        }
    }
}
