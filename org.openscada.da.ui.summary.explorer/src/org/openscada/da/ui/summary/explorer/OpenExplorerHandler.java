/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.da.ui.summary.explorer;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.openscada.da.connection.provider.ConnectionService;
import org.openscada.ui.databinding.AbstractSelectionHandler;
import org.openscada.ui.databinding.SelectionHelper;

public class OpenExplorerHandler extends AbstractSelectionHandler
{

    private int counter;

    @Override
    public Object execute ( final ExecutionEvent event ) throws ExecutionException
    {
        try
        {
            // the following cast might look a bit weird. But first an adapter is requested and it only adapts to "core" connection services.
            final ConnectionService connectionService = (ConnectionService)SelectionHelper.first ( getSelection (), org.openscada.core.connection.provider.ConnectionService.class );
            final IViewPart view = getActivePage ().showView ( SummaryExplorerViewPart.VIEW_ID, "" + this.counter++, IWorkbenchPage.VIEW_ACTIVATE );

            ( (SummaryExplorerViewPart)view ).setConnectionService ( connectionService );
        }
        catch ( final PartInitException e )
        {
            throw new ExecutionException ( "Failed to open view", e );
        }
        return null;
    }

}
