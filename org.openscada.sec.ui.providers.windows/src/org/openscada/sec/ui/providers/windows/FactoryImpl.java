/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.sec.ui.providers.windows;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.list.WritableList;
import org.eclipse.ui.statushandlers.StatusManager;
import org.openscada.sec.ui.providers.KeyProviderFactory;
import org.openscada.ui.utils.status.StatusHelper;

public class FactoryImpl implements KeyProviderFactory
{

    private final Set<String> names = new HashSet<String> ();

    private Realm realm;

    private WritableList list;

    public FactoryImpl ()
    {
        try
        {
            final String[] tokens = System.getProperty ( "org.openscada.sec.ui.providers.windows.defaultStoreNames", "Windows-MY,Windows-ROOT" ).split ( "\\s*,\\s*" );
            this.names.addAll ( Arrays.asList ( tokens ) );
        }
        catch ( final Exception e )
        {
            this.names.clear ();
            this.names.add ( "Windows-MY" );
            this.names.add ( "Windows-ROOT" );
        }
    }

    @Override
    public void init ( final Realm realm )
    {
        this.realm = realm;
        this.list = new WritableList ();

        for ( final String name : this.names )
        {
            try
            {
                this.list.add ( new SunMSCAPIProvider ( this.realm, name ) );
            }
            catch ( final Exception e )
            {
                StatusManager.getManager ().handle ( StatusHelper.convertStatus ( Activator.PLUGIN_ID, e ) );
            }
        }
    }

    @Override
    public void dispose ()
    {
        for ( final Object o : this.list )
        {
            ( (SunMSCAPIProvider)o ).dispose ();
        }
        this.list.dispose ();
    }

    @Override
    public String toString ()
    {
        return "SunMSCAPI";
    }

    @Override
    public IObservableList getKeyProviders ()
    {
        return this.list;
    }

}
