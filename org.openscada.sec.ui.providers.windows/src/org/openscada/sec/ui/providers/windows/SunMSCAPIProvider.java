/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.sec.ui.providers.windows;

import java.security.KeyStore;

import org.eclipse.core.databinding.observable.Realm;
import org.openscada.sec.callback.CallbackHandler;
import org.openscada.sec.ui.providers.AbstractKeyStoreKeyProvider;

public class SunMSCAPIProvider extends AbstractKeyStoreKeyProvider
{

    private final String name;

    public SunMSCAPIProvider ( final Realm realm, final String name ) throws Exception
    {
        super ( realm );
        this.name = name;

        setKeyStore ( KeyStore.getInstance ( name, "SunMSCAPI" ) );
        try
        {
            // try to load initially
            performLoad ();
        }
        catch ( final Exception e )
        {
            // silently fail
        }
    }

    @Override
    public String toString ()
    {
        return String.format ( "%s", this.name );
    }

    @Override
    public void unlock ( final CallbackHandler callbackHandler ) throws Exception
    {
        load ();
    }

}
