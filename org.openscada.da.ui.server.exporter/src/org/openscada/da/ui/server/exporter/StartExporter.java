/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.da.ui.server.exporter;

import java.net.URI;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.openscada.da.server.exporter.osgi.Exporter;
import org.openscada.ui.databinding.AbstractSelectionHandler;
import org.openscada.ui.databinding.SelectionHelper;

public class StartExporter extends AbstractSelectionHandler
{

    @Override
    public Object execute ( final ExecutionEvent event ) throws ExecutionException
    {
        for ( final IFile file : SelectionHelper.iterable ( getSelection (), IFile.class ) )
        {
            processFile ( file.getLocationURI () );
        }

        return null;
    }

    private void processFile ( final URI uri ) throws ExecutionException
    {
        try
        {
            final Exporter exporter = new Exporter ( uri.toString () );
            exporter.start ();
        }
        catch ( final Exception e )
        {
            throw new ExecutionException ( "Failed to start exporter", e );
        }

    }
}
