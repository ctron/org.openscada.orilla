/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.core.ui.connection.information.details;

import org.eclipse.jface.databinding.viewers.ObservableSetContentProvider;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.openscada.core.connection.provider.ConnectionService;
import org.openscada.core.connection.provider.info.ConnectionInformationProvider;
import org.openscada.core.ui.connection.information.ConnectionInformationWrapper;
import org.openscada.core.ui.connection.information.InformationBeanComparator;
import org.openscada.core.ui.connection.information.LabelProvider;

public class StatisticsTab implements DetailsTab
{

    private TabItem item;

    private ConnectionInformationWrapper wrapper;

    private Object provider;

    private TableViewer viewer;

    private ObservableSetContentProvider contentProvider;

    @Override
    public void setSelection ( final ConnectionService connectionService )
    {
        if ( connectionService instanceof ConnectionInformationProvider )
        {
            setProvider ( (ConnectionInformationProvider)connectionService );
        }
    }

    private void setProvider ( final ConnectionInformationProvider provider )
    {
        if ( this.wrapper != null )
        {
            this.viewer.setInput ( null );
            this.wrapper.dispose ();
            this.wrapper = null;
        }
        this.provider = provider;
        if ( this.provider != null )
        {
            this.wrapper = new ConnectionInformationWrapper ( provider );

            this.viewer.setInput ( this.wrapper );
        }
    }

    protected void handleDispose ()
    {
        setProvider ( null );
    }

    @Override
    public void createTab ( final TabFolder tabFolder )
    {
        this.item = new TabItem ( tabFolder, SWT.NONE );
        this.item.setText ( Messages.StatisticsTab_TabItem_Label );
        this.item.addDisposeListener ( new DisposeListener () {

            @Override
            public void widgetDisposed ( final DisposeEvent e )
            {
                handleDispose ();
            }
        } );

        this.viewer = new TableViewer ( tabFolder, SWT.NONE );
        this.contentProvider = new ObservableSetContentProvider ();
        this.viewer.setContentProvider ( this.contentProvider );

        this.viewer.getTable ().setHeaderVisible ( true );

        final TableLayout layout = new TableLayout ();
        this.viewer.getTable ().setLayout ( layout );

        {
            final TableViewerColumn col = new TableViewerColumn ( this.viewer, SWT.NONE );
            col.setLabelProvider ( new LabelProvider ( this.contentProvider.getRealizedElements () ) );
            layout.addColumnData ( new ColumnWeightData ( 100 ) );
        }
        {
            final TableViewerColumn col = new TableViewerColumn ( this.viewer, SWT.NONE );
            col.setLabelProvider ( new LabelProvider ( this.contentProvider.getRealizedElements () ) );
            layout.addColumnData ( new ColumnWeightData ( 50 ) );
            col.getColumn ().setText ( Messages.StatisticsTab_Col_Current_Label );
        }
        {
            final TableViewerColumn col = new TableViewerColumn ( this.viewer, SWT.NONE );
            col.setLabelProvider ( new LabelProvider ( this.contentProvider.getRealizedElements () ) );
            layout.addColumnData ( new ColumnWeightData ( 50 ) );
            col.getColumn ().setText ( Messages.StatisticsTab_Col_Min_Label );
        }
        {
            final TableViewerColumn col = new TableViewerColumn ( this.viewer, SWT.NONE );
            col.setLabelProvider ( new LabelProvider ( this.contentProvider.getRealizedElements () ) );
            layout.addColumnData ( new ColumnWeightData ( 50 ) );
            col.getColumn ().setText ( Messages.StatisticsTab_Col_Max_Label );
        }

        this.viewer.setComparator ( new InformationBeanComparator () );

        this.item.setControl ( this.viewer.getControl () );
    }

}
