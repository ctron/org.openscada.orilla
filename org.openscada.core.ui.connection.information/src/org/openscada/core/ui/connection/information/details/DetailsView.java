/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.core.ui.connection.information.details;

import java.util.LinkedHashSet;
import java.util.Set;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TabFolder;
import org.openscada.core.connection.provider.ConnectionService;
import org.openscada.ui.databinding.SelectionHelper;

public class DetailsView
{

    private final TabFolder tabFolder;

    private final Set<DetailsTab> tabs = new LinkedHashSet<DetailsTab> ();

    public DetailsView ( final Composite parent )
    {
        this.tabFolder = new TabFolder ( parent, SWT.NONE );

        this.tabs.add ( new InterfacesTab () );
        this.tabs.add ( new StatisticsTab () );
        this.tabs.add ( new PrivilegeTab () );
        this.tabs.add ( new PropertiesTab () );

        for ( final DetailsTab tab : this.tabs )
        {
            tab.createTab ( this.tabFolder );
        }
    }

    public void setFocus ()
    {
        this.tabFolder.setFocus ();
    }

    public void setSelection ( final ISelection selection )
    {
        final ConnectionService connectionService = SelectionHelper.first ( selection, ConnectionService.class );
        for ( final DetailsTab tab : this.tabs )
        {
            tab.setSelection ( connectionService );
        }
    }

}
