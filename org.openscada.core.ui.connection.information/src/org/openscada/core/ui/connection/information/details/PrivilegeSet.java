/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.core.ui.connection.information.details;

import java.util.Set;

import org.eclipse.core.databinding.observable.Diffs;
import org.eclipse.core.databinding.observable.DisposeEvent;
import org.eclipse.core.databinding.observable.IDisposeListener;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.set.WritableSet;
import org.openscada.core.client.Connection;
import org.openscada.core.client.PrivilegeListener;

public class PrivilegeSet extends WritableSet
{

    private final Connection connection;

    private final PrivilegeListener listener = new PrivilegeListener () {

        @Override
        public void privilegesChanged ( final Set<String> granted )
        {
            PrivilegeSet.this.privilegesChanged ( granted );
        }
    };

    public PrivilegeSet ( final Connection connection, final Realm realm )
    {
        super ( realm );

        this.connection = connection;
        addDisposeListener ( new IDisposeListener () {

            @Override
            public void handleDispose ( final DisposeEvent event )
            {
                PrivilegeSet.this.handleDispose ();
            }
        } );
        connection.addPrivilegeListener ( this.listener );
    }

    private void handleDispose ()
    {
        this.connection.removePrivilegeListener ( this.listener );
    }

    private void privilegesChanged ( final Set<String> granted )
    {
        if ( isDisposed () )
        {
            return;
        }

        getRealm ().asyncExec ( new Runnable () {

            @Override
            public void run ()
            {
                handleChange ( granted );
            }
        } );
    }

    private void handleChange ( final Set<String> granted )
    {
        if ( isDisposed () )
        {
            return;
        }

        try
        {
            setStale ( false );

            if ( granted == null )
            {
                clear ();
            }
            else
            {
                Diffs.computeSetDiff ( this, granted ).applyTo ( this );
            }
        }
        finally
        {
            setStale ( false );
        }
    }
}
