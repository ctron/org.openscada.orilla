/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.core.ui.connection.information.details;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.openscada.core.connection.provider.ConnectionService;

public class InterfacesTab implements DetailsTab
{
    private TabItem item;

    private ListViewer viewer;

    @Override
    public void createTab ( final TabFolder tabFolder )
    {
        this.item = new TabItem ( tabFolder, SWT.NONE );
        this.item.setText ( Messages.InterfacesTab_TabItem_Label );

        this.viewer = new ListViewer ( tabFolder );
        this.item.setControl ( this.viewer.getControl () );
        this.viewer.setContentProvider ( new ArrayContentProvider () );
    }

    @Override
    public void setSelection ( final ConnectionService connectionService )
    {
        if ( connectionService == null )
        {
            this.viewer.setInput ( null );
        }
        else
        {
            this.viewer.setInput ( connectionService.getSupportedInterfaces () );
        }
    }
}
