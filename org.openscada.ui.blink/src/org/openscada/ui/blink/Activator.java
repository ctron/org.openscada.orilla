/*
 * This file is part of the OpenSCADA project
 * 
 * Copyright (C) 2006-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * OpenSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.ui.blink;

import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.openscada.ui.blink.internal.DisplayBlinkServiceImpl;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin
{

    // The plug-in ID
    public static final String PLUGIN_ID = "org.openscada.ui.blink"; //$NON-NLS-1$

    // The shared instance
    private static Activator plugin;

    private DisplayBlinkServiceImpl toggle;

    /**
     * The constructor
     */
    public Activator ()
    {
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
     */
    @Override
    public void start ( final BundleContext context ) throws Exception
    {
        super.start ( context );
        plugin = this;

        this.toggle = new DisplayBlinkServiceImpl ( getWorkbench ().getDisplay () );

        Display.getDefault ().asyncExec ( new Runnable () {

            @Override
            public void run ()
            {
                Activator.this.toggle.start ();
            }
        } );
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
     */
    @Override
    public void stop ( final BundleContext context ) throws Exception
    {
        Display.getDefault ().asyncExec ( new Runnable () {

            @Override
            public void run ()
            {
                Activator.this.toggle.stop ();
            }
        } );

        this.toggle = null;

        plugin = null;
        super.stop ( context );
    }

    /**
     * Returns the shared instance
     * 
     * @return the shared instance
     */
    public static Activator getDefault ()
    {
        return plugin;
    }

    private void addToggle ( final BlinkCallback callback )
    {
        this.toggle.addListener ( callback );
    }

    private void removeToggle ( final BlinkCallback callback )
    {
        this.toggle.removeListener ( callback );
    }

    public static void addDefaultToggle ( final BlinkCallback callback )
    {
        getDefault ().addToggle ( callback );
    }

    public static void removeDefaultToggle ( final BlinkCallback callback )
    {
        final Activator defaultInstance = getDefault ();
        if ( defaultInstance != null )
        {
            defaultInstance.removeToggle ( callback );
        }
    }

}
